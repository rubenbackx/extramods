package rubenbackx.extraStuff.client;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;

public class RenderWandItem implements IItemRenderer{

	private final ModelWand model;
	
	public RenderWandItem() {
		this.model = new ModelWand();
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		GL11.glPushMatrix();
		
		switch (type) {
			case EQUIPPED:				
				GL11.glTranslatef(0.9F, 1.2F, 0.8F);			
				GL11.glRotatef(180, 1F, -1F, 0F);
				break;
			case EQUIPPED_FIRST_PERSON:
				GL11.glTranslatef(0F, 1F, 0.5F);				
				GL11.glRotatef(180, 1F, -1F, 0F);
				break;
			default:
		}
		
		GL11.glRotatef(180, 1, 0, 0);
		GL11.glScalef(0.85F, 0.85F, 0.85F);
        GL11.glTranslatef(-0.25F, -0.2F, -0.2F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(RenderWand.location);
        this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		
		GL11.glPopMatrix();
	}

}
