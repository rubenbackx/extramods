package rubenbackx.extraStuff.client;

import net.minecraft.client.model.ModelCreeper;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderCreeperBlock extends TileEntitySpecialRenderer {
        
        private final ModelCreeper model;
        public static final ResourceLocation location = new ResourceLocation("extrastuff", "textures/blocks/creeper.png");
        
        public RenderCreeperBlock() {
                this.model = new ModelCreeper();
        }
        
        @Override
        public void renderTileEntityAt(TileEntity te, double x, double y, double z, float scale) {
        	GL11.glPushMatrix();
        	
            GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
            
            bindTexture(this.location);
            
            GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
            
            this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
            
            GL11.glPopMatrix();
        }
}