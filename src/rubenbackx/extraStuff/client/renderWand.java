package rubenbackx.extraStuff.client;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import rubenbackx.extraStuff.tileEntities.TileEntityWand;

public class RenderWand extends TileEntitySpecialRenderer {
        
        private final ModelWand model;
        public static final ResourceLocation location = new ResourceLocation("extrastuff", "textures/blocks/wandModel.png");
        
        public RenderWand() {
                this.model = new ModelWand();
        }
        
        @Override
        public void renderTileEntityAt(TileEntity te, double x, double y, double z, float scale) {
        	GL11.glPushMatrix();
        	
            GL11.glTranslatef((float) x + 0.433F, (float) y + 1.5F, (float) z + 0.433F);
            
            bindTexture(this.location); 
            
            GL11.glRotatef(270F, 0.0F, 0.0F, 1.0F);
            
            this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
            
            GL11.glPopMatrix();
        }
     
}