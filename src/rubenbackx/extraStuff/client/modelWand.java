package rubenbackx.extraStuff.client;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelWand extends ModelBase
{
  //fields
    ModelRenderer rod;
    ModelRenderer topBase;
    ModelRenderer topPiece1;
    ModelRenderer topPiece2;
    ModelRenderer topPiece3;
    ModelRenderer topPiece4;
    ModelRenderer topPiece5;
    ModelRenderer topPiece6;
    ModelRenderer topPiece7;
    ModelRenderer topPiece8;
  
  public ModelWand()
  {
    textureWidth = 64;
    textureHeight = 32;
    
      rod = new ModelRenderer(this, 0, 14);
      rod.addBox(0F, 0F, 0F, 24, 2, 2);
      rod.setRotationPoint(0F, 0F, 0F);
      rod.setTextureSize(64, 32);
      rod.mirror = true;
      setRotation(rod, 0F, 0F, 0F);
      topBase = new ModelRenderer(this, 0, 0);
      topBase.addBox(0F, 0F, 0F, 10, 6, 6);
      topBase.setRotationPoint(-10F, -2F, -2F);
      topBase.setTextureSize(64, 32);
      topBase.mirror = true;
      setRotation(topBase, 0F, 0F, 0F);
      topPiece1 = new ModelRenderer(this, 0, 12);
      topPiece1.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece1.setRotationPoint(-9F, -1F, -3F);
      topPiece1.setTextureSize(64, 32);
      topPiece1.mirror = true;
      setRotation(topPiece1, 0F, 0F, 0F);
      topPiece2 = new ModelRenderer(this, 0, 12);
      topPiece2.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece2.setRotationPoint(-9F, -3F, -1F);
      topPiece2.setTextureSize(64, 32);
      topPiece2.mirror = true;
      setRotation(topPiece2, 0F, 0F, 0F);
      topPiece3 = new ModelRenderer(this, 0, 12);
      topPiece3.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece3.setRotationPoint(-9F, -3F, 2F);
      topPiece3.setTextureSize(64, 32);
      topPiece3.mirror = true;
      setRotation(topPiece3, 0F, 0F, 0F);
      topPiece4 = new ModelRenderer(this, 0, 12);
      topPiece4.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece4.setRotationPoint(-9F, -1F, 4F);
      topPiece4.setTextureSize(64, 32);
      topPiece4.mirror = true;
      setRotation(topPiece4, 0F, 0F, 0F);
      topPiece5 = new ModelRenderer(this, 0, 12);
      topPiece5.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece5.setRotationPoint(-9F, 2F, 4F);
      topPiece5.setTextureSize(64, 32);
      topPiece5.mirror = true;
      setRotation(topPiece5, 0F, 0F, 0F);
      topPiece6 = new ModelRenderer(this, 0, 12);
      topPiece6.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece6.setRotationPoint(-9F, 4F, 2F);
      topPiece6.setTextureSize(64, 32);
      topPiece6.mirror = true;
      setRotation(topPiece6, 0F, 0F, 0F);
      topPiece7 = new ModelRenderer(this, 0, 12);
      topPiece7.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece7.setRotationPoint(-9F, 4F, -1F);
      topPiece7.setTextureSize(64, 32);
      topPiece7.mirror = true;
      setRotation(topPiece7, 0F, 0F, 0F);
      topPiece8 = new ModelRenderer(this, 0, 12);
      topPiece8.addBox(0F, 0F, 0F, 8, 1, 1);
      topPiece8.setRotationPoint(-9F, 2F, -3F);
      topPiece8.setTextureSize(64, 32);
      topPiece8.mirror = true;
      setRotation(topPiece8, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    rod.render(f5);
    topBase.render(f5);
    topPiece1.render(f5);
    topPiece2.render(f5);
    topPiece3.render(f5);
    topPiece4.render(f5);
    topPiece5.render(f5);
    topPiece6.render(f5);
    topPiece7.render(f5);
    topPiece8.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
