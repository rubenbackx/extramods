package rubenbackx.extraStuff.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelCreeper;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

import org.lwjgl.opengl.GL11;

public class RenderCreeperBlockItem implements IItemRenderer{

	private final ModelCreeper model;
	
	public RenderCreeperBlockItem() {
		this.model = new ModelCreeper();
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		GL11.glPushMatrix();
		
		switch (type) {
			case EQUIPPED:				
				GL11.glTranslatef(0.5F, 0.7F, 0.6F);
				GL11.glRotatef(220, 0, 1, 0);
				GL11.glScalef(2, 2, 2);
				break;
			case EQUIPPED_FIRST_PERSON:
				GL11.glTranslatef(0F, 1F, 0.5F);				
				GL11.glRotatef(270, 0F, 1F, 0);
				break;
			default:
		}
		
		GL11.glRotatef(180, 1, 0, 0);
        GL11.glTranslatef(0F, -0.45F, 0F);
        GL11.glScalef(0.8F, 0.8F, 0.8F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(RenderCreeperBlock.location);
        this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		
		GL11.glPopMatrix();
	}

}
