package rubenbackx.extraStuff.client;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelNuclearBomb extends ModelBase
{
  //fields
    ModelRenderer body;
    ModelRenderer top;
    ModelRenderer wing1;
    ModelRenderer wing2;
    ModelRenderer wing3;
    ModelRenderer wing4;
  
  public ModelNuclearBomb()
  {
    textureWidth = 128;
    textureHeight = 64;
    
      body = new ModelRenderer(this, 0, 0);
      body.addBox(0F, 0F, 0F, 12, 16, 12);
      body.setRotationPoint(-6F, 5F, -6F);
      body.setTextureSize(128, 64);
      body.mirror = true;
      setRotation(body, 0F, 0F, 0F);
      top = new ModelRenderer(this, 0, 28);
      top.addBox(0F, 0F, 0F, 8, 2, 8);
      top.setRotationPoint(-4F, 3F, -4F);
      top.setTextureSize(128, 64);
      top.mirror = true;
      setRotation(top, 0F, 0F, 0F);
      wing1 = new ModelRenderer(this, 48, 0);
      wing1.addBox(0F, 0F, 0F, 3, 5, 2);
      wing1.setRotationPoint(-8F, 19F, -1F);
      wing1.setTextureSize(128, 64);
      wing1.mirror = true;
      setRotation(wing1, 0F, 0F, 0F);
      wing2 = new ModelRenderer(this, 48, 0);
      wing2.addBox(0F, 0F, 0F, 3, 5, 2);
      wing2.setRotationPoint(5F, 19F, -1F);
      wing2.setTextureSize(128, 64);
      wing2.mirror = true;
      setRotation(wing2, 0F, 0F, 0F);
      wing3 = new ModelRenderer(this, 48, 0);
      wing3.addBox(0F, 0F, 0F, 2, 5, 3);
      wing3.setRotationPoint(-1F, 19F, 5F);
      wing3.setTextureSize(128, 64);
      wing3.mirror = true;
      setRotation(wing3, 0F, 0F, 0F);
      wing4 = new ModelRenderer(this, 48, 0);
      wing4.addBox(0F, 0F, 0F, 2, 5, 3);
      wing4.setRotationPoint(-1F, 19F, -8F);
      wing4.setTextureSize(128, 64);
      wing4.mirror = true;
      setRotation(wing4, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    body.render(f5);
    top.render(f5);
    wing1.render(f5);
    wing2.render(f5);
    wing3.render(f5);
    wing4.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
