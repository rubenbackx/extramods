package rubenbackx.extraStuff.client;

import rubenbackx.extraStuff.blocks.BlockUraniumOre;
import rubenbackx.extraStuff.blocks.Blocks;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.world.World;

public class EntityUraniumParticle extends EntityFX {

	public EntityUraniumParticle(World world, double x, double y, double z, double motionX, double motionY, double motionZ) {
		super(world, x, y, z, motionX, motionY, motionZ);	
		
		setParticleIcon(BlockUraniumOre.particleIcon);
		
		particleScale = 0.8F;
		particleAlpha = rand.nextFloat();
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		
		particleScale = (1 - (float)particleAge / particleMaxAge) * 0.8F;
	}
	
	@Override
	public int getFXLayer() {
		return 1;
	}
	
}