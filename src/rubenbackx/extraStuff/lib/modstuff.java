package rubenbackx.extraStuff.lib;

public class Modstuff {
	
	//Mod
	public static final String MOD_ID = "ExtraStuff";
	public static final String MOD_NAME = "Extra Stuff++";
	public static final String MOD_VERSION = "Beta";
	
	//Networkmod
	public static final String MOD_CHANNEL = "ExtraStuff";
	public static final boolean MOD_CLIENTSIDEREQ = true;
	public static final boolean MOD_SERVERSIDEREQ = false;

}
