package rubenbackx.extraStuff.worldGen;

import java.util.Random;

import rubenbackx.extraStuff.blocks.BlockInfo;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;

public class WorldGenerationHandlerOres implements IWorldGenerator{

	public WorldGenerator uraniumGen;
	public WorldGenerator leadGen;
	
	public WorldGenerationHandlerOres() {
		GameRegistry.registerWorldGenerator(this);
		uraniumGen = new WorldGenMinable(BlockInfo.URANIUM_ORE_ID, 16);
		leadGen = new WorldGenMinable(BlockInfo.LEAD_ORE_ID, 16);
	}
	
	@Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		generateStandardOre(random, chunkX, chunkZ, world, 2, uraniumGen, 6, 7);
		generateStandardOre(random, chunkX, chunkZ, world, 8, leadGen, 6, 20);
    }
	
	private void generateStandardOre(Random random, int chunkX, int chunkZ, World world, int iterations, WorldGenerator gen, int lowestY, int highestY){
		for (int i = 0; i < iterations; i++) {
			int x = chunkX * 16 + random.nextInt(16);
			int y = random.nextInt(highestY - lowestY) + lowestY;
			int z = chunkZ * 16 + random.nextInt(16);
			
			gen.generate(world, random, x, y, z);
		}
	}

}
