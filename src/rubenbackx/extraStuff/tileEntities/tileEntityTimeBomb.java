package rubenbackx.extraStuff.tileEntities;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileEntityTimeBomb extends TileEntity{
	
	private int timer;
	private boolean isOff;
	
	public TileEntityTimeBomb(){
		
		timer = 300;
		isOff = false;
		
	}
	
	@Override
	public void updateEntity(){
		
		if(!worldObj.isRemote){
			if(worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord)){
				
				if(timer == 0){
						
					worldObj.setBlockToAir(xCoord, yCoord, zCoord);
					worldObj.createExplosion(null, xCoord + 0.5, yCoord + 0.5, zCoord + 0.5, 16, true);
				}
				
				timer--;
				isOff = true;
	
				if(timer%20 == 0){
				
					//System.out.println(timer/20);
					
					if(timer>=280){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 14, 3);
					}else if(timer<=280&&timer>=260){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 13, 3);
					}else if(timer<=260&&timer>=240){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 12, 3);
					}else if(timer<=240&&timer>=220){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 11, 3);
					}else if(timer<=220&&timer>=200){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 10, 3);
					}else if(timer<=200&&timer>=180){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 9, 3);
					}else if(timer<=180&&timer>=160){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 8, 3);
					}else if(timer<=160&&timer>=140){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 7, 3);
					}else if(timer<=140&&timer>=120){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 6, 3);
					}else if(timer<=120&&timer>=100){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 5, 3);
					}else if(timer<=100&&timer>=80){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 4, 3);
					}else if(timer<=80&&timer>=60){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 3, 3);
					}else if(timer<=60&&timer>=40){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 2, 3);
					}else if(timer<=40&&timer>=20){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 1, 3);
					}else if(timer<=20&&timer>=0){
						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 1, 3);
					}
				}
			
			}else{
				
				worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 15, 3);
				
				isOff = false;
				timer = 300;
				
			}
		
  		}else{
  			
//			if(worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord)){
//				
//				timer--;
//				isOff = true;
//				
//				System.out.println(timer);
//				
//				if(timer%20 == 0){
//				
//					System.out.println(timer/20);
//					
//					if(timer>=280){
//						this.playSound("note.pling", 0);
//					}else if(timer<=280&&timer>=260){
//						this.playSound("note.pling", 0.2F);
//					}else if(timer<=260&&timer>=240){
//						this.playSound("note.pling", 0.4F);
//					}else if(timer<=240&&timer>=220){
//						this.playSound("note.pling", 0.6F);
//					}else if(timer<=220&&timer>=200){
//						this.playSound("note.pling", 0.8F);
//					}else if(timer<=200&&timer>=180){
//						this.playSound("note.pling", 1);
//					}else if(timer<=180&&timer>=160){
//						this.playSound("note.pling", 1.2F);
//					}else if(timer<=160&&timer>=140){
//						this.playSound("note.pling", 1.4F);
//					}else if(timer<=140&&timer>=120){
//						this.playSound("note.pling", 1.6F);
//					}else if(timer<=120&&timer>=100){
//						this.playSound("note.pling", 1.8F);
//					}else if(timer<=100&&timer>=80){
//						this.playSound("note.pling", 2F);
//					}else if(timer<=80&&timer>=60){
//						this.playSound("note.pling", 2.2F);
//					}else if(timer<=60&&timer>=40){
//						this.playSound("note.pling", 2.4F);
//					}else if(timer<=40&&timer>=20){
//						this.playSound("note.pling", 2.6F);
//					}else if(timer<=20&&timer>=0){
//						this.playSound("note.pling", 2.8F);
//					}
//				}
//			
//			}else{
//				
//				if(isOff){
//					this.playSound("random.fizz", 0.5F);
//				}
//				
//				timer = 300;
//				isOff = false;
//				
//			}
			
		}
		
		super.updateEntity();
		
	}
	
	@SideOnly(Side.CLIENT)
	public void playSound(String sound, float pitch){
		
		Minecraft.getMinecraft().sndManager.playSound(sound, xCoord, yCoord, zCoord, 1, pitch);
		
	}

	@Override
	public void writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		
		compound.setShort("Timer", (short)timer);
		compound.setBoolean("Off", (boolean)isOff);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		
		timer = compound.getShort("Timer");
		isOff = compound.getBoolean("Off");
	}
	
}
