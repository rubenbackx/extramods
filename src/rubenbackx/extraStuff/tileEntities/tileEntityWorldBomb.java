package rubenbackx.extraStuff.tileEntities;

import rubenbackx.extraStuff.blocks.BlockInfo;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityWorldBomb extends TileEntity{

	private int timer;
	
	public TileEntityWorldBomb(){
		
		timer = 30;
		
	}
	
	@Override
	public void updateEntity(){
		
		if(!worldObj.isRemote){
			
			if(timer == 0){
				spread(xCoord+1, yCoord, zCoord);
				spread(xCoord-1, yCoord, zCoord);
				spread(xCoord, yCoord+1, zCoord);
				spread(xCoord, yCoord-1, zCoord);
				spread(xCoord, yCoord, zCoord+1);
				spread(xCoord, yCoord, zCoord-1);
				
				worldObj.setBlockToAir(xCoord, yCoord, zCoord);
					
			}
			
			if(worldObj.getBlockMetadata(xCoord, yCoord, zCoord) == 0){
			
				timer--;
			
			}
			
		}
		
	}
	
	private void spread(int x, int y, int z){
		
		if (!worldObj.isAirBlock(x, y, z)) {
			worldObj.setBlock(x, y, z, BlockInfo.BOMB_ID);
			worldObj.setBlockMetadataWithNotify(x, y, z, 0, 3);
			
			TileEntity tile = worldObj.getBlockTileEntity(x, y, z);
			
		}
		
	}
	
	@Override
	public void writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		
		compound.setShort("Timer", (short)timer);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		
		timer = compound.getShort("Timer");
	}
}
