package rubenbackx.extraStuff.inventory;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import rubenbackx.extraStuff.tileEntities.TileEntityAnvilTrap;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiAnvilTrap extends GuiContainer{

	private TileEntityAnvilTrap te;
	
	public GuiAnvilTrap(InventoryPlayer inv, TileEntityAnvilTrap te) {
		super(new ContainerAnvilTrap(inv, te));
		
		this.te = te;
	}

	private static final ResourceLocation texture = new ResourceLocation("extrastuff", "textures/gui/anvilTrap.png");
	
	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y){
		
		fontRenderer.drawString("Anvil Trap", 8, 6, 0x404040);
        fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 126, 4210752);
		
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int x, int y) {
		GL11.glColor4f(1, 1, 1, 1);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);		
	}

}
