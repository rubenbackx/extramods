package rubenbackx.extraStuff.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import rubenbackx.extraStuff.tileEntities.TileEntityAnvilTrap;

public class ContainerAnvilTrap extends Container{

	private TileEntityAnvilTrap te;
	
	public ContainerAnvilTrap(InventoryPlayer inv, TileEntityAnvilTrap te){
		
		this.te = te;
		
		for (int x = 0; x < 9; x++) {
			addSlotToContainer(new Slot(inv, x, 8 + 18 * x, 109));
		}
		
		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 9; x++) {
				addSlotToContainer(new Slot(inv, x + y * 9 + 9, 8 + 18 * x, 51 + y * 18));
			}
		}
		
		addSlotToContainer(new SlotAnvil(te, 0, 80, 20));
		
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return te.isUseableByPlayer(entityplayer);
	}
	
	@Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
		
        ItemStack stack = null;
        
        Slot slotObject = (Slot) inventorySlots.get(slot);

        if (slotObject != null && slotObject.getHasStack()) {
            
        	ItemStack stackInSlot = slotObject.getStack();
            stack = stackInSlot.copy();

            if (slot < 9) {
               	
            	if (!this.mergeItemStack(stackInSlot, 0, 35, true)) {
            	
            		return null;
            	
            	}
                	
            }else if(!this.mergeItemStack(stackInSlot, 0, 9, false)) {
                    
                	return null;
                
            }

            if (stackInSlot.stackSize == 0) {
                    
            	slotObject.putStack(null);
                
            } else {
                    
                slotObject.onSlotChanged();
                
            }

            if (stackInSlot.stackSize == stack.stackSize) {
                    
             	return null;
                
            }
            
            slotObject.onPickupFromSlot(player, stackInSlot);
        
        }
            
        return stack;
    }

}
