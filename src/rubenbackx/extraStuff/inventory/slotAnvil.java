package rubenbackx.extraStuff.inventory;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import net.minecraft.block.Block;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotAnvil extends Slot{

	public SlotAnvil(IInventory inv, int id, int x, int y) {
		super(inv, id, x, y);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack) {
		
		return stack.itemID == Block.anvil.blockID && stack.getItemDamage() == 0 && stack.stackSize <= 64;

	}
}
