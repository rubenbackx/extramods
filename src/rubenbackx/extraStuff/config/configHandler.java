package rubenbackx.extraStuff.config;

import java.io.File;

import rubenbackx.extraStuff.blocks.BlockInfo;
import rubenbackx.extraStuff.items.ItemInfo;
import net.minecraftforge.common.Configuration;

public class ConfigHandler {
	
	public static void init(File file){
		
		Configuration config = new Configuration(file);
		
		config.load();
		
		BlockInfo.BOMB_ID = config.getBlock(BlockInfo.BOMB_KEY, BlockInfo.BOMB_DEFAULT).getInt() - 256;
		BlockInfo.CREEPER_BLOCK_ID = config.getBlock(BlockInfo.CREEPER_KEY, BlockInfo.CREEPER_DEFAULT).getInt() - 256;
		BlockInfo.DICE_ID = config.getBlock(BlockInfo.DICE_KEY, BlockInfo.DICE_DEFAULT).getInt() - 256;
		BlockInfo.DOOM_ID = config.getBlock(BlockInfo.DOOM_KEY, BlockInfo.DOOM_DEFAULT).getInt() - 256;
		BlockInfo.FRAME_ID = config.getBlock(BlockInfo.FRAME_KEY, BlockInfo.FRAME_DEFAULT).getInt() - 256;
		BlockInfo.NUKE_ID = config.getBlock(BlockInfo.NUKE_KEY, BlockInfo.NUKE_DEFAULT).getInt() - 256;
		BlockInfo.POOP_ID = config.getBlock(BlockInfo.POOP_KEY, BlockInfo.POOP_DEFAULT).getInt() - 256;
		BlockInfo.TIMEBOMB_ID = config.getBlock(BlockInfo.TIMEBOMB_KEY, BlockInfo.TIMEBOMB_DEFAULT).getInt() - 256;
		BlockInfo.TRAP_ID = config.getBlock(BlockInfo.TRAP_KEY, BlockInfo.TRAP_DEFAULT).getInt() - 256;
		BlockInfo.URANIUM_ORE_ID = config.getBlock(BlockInfo.URANIUM_ORE_KEY, BlockInfo.URANIUM_ORE_DEFAULT).getInt() - 256;
		BlockInfo.WAND_BLOCK_ID = config.getBlock(BlockInfo.WAND_KEY, BlockInfo.WAND_DEFAULT).getInt() - 256;
		BlockInfo.URANIUM_BLOCK_ID = config.getBlock(BlockInfo.URANIUM_KEY, BlockInfo.URANIUM_DEFAULT).getInt() - 256;
		BlockInfo.LEAD_ORE_ID = config.getBlock(BlockInfo.LEAD_ORE_KEY, BlockInfo.LEAD_ORE_DEFAULT).getInt() - 256;
		
		ItemInfo.WAND_ID = config.getItem(ItemInfo.WAND_KEY, ItemInfo.WAND_DEFAULT).getInt() - 256;
		ItemInfo.URANIUM_ID = config.getItem(ItemInfo.URANIUM_KEY, ItemInfo.URANIUM_DEFAULT).getInt() - 256;
		ItemInfo.LEAD_INGOT_ID = config.getItem(ItemInfo.LEAD_KEY, ItemInfo.LEAD_DEFAULT).getInt() - 256;
		ItemInfo.LEAD_PLATE_ID = config.getItem(ItemInfo.LEAD_PLATE_KEY, ItemInfo.LEAD_PLATE_DEFAULT).getInt() - 256;
		
		BlockInfo.NUKE_ENABLED = config.get("Enable", "Nuke enabled", true).getBoolean(true);
		BlockInfo.BOMB_ENABLED = config.get("Enable", "Enable world bomb", true).getBoolean(true);
		
		config.save();
		
	}

}
