package rubenbackx.extraStuff.recipes;

import rubenbackx.extraStuff.blocks.Blocks;
import rubenbackx.extraStuff.items.Items;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemStacks {

	public static final ItemStack diamondStack = new ItemStack(Item.diamond);
	public static final ItemStack blazeStack = new ItemStack(Item.blazeRod);
	public static final ItemStack pistonStack = new ItemStack(Block.pistonBase);
	public static final ItemStack plateStack = new ItemStack(Block.pressurePlateStone);
	public static final ItemStack anvilStack = new ItemStack(Block.anvil);
	public static final ItemStack ironStack = new ItemStack(Item.ingotIron);
	public static final ItemStack redstoneStack = new ItemStack(Item.redstone);
	public static final ItemStack lapisStack = new ItemStack(Item.dyePowder, 1, 4);
	public static final ItemStack starStack = new ItemStack(Item.netherStar);
	public static final ItemStack dirtStack = new ItemStack(Block.dirt);
	public static final ItemStack cacoaStack = new ItemStack(Item.dyePowder, 1, 3);
	public static final ItemStack waterStack = new ItemStack(Item.bucketWater);
	public static final ItemStack TNTStack = new ItemStack(Block.tnt);
	public static final ItemStack goldStack = new ItemStack(Item.ingotGold);
	public static final ItemStack clockStack = new ItemStack(Item.pocketSundial);
	public static final ItemStack wandStack = new ItemStack(Items.wand);
	public static final ItemStack uraniumStack = new ItemStack(Items.uranium);
	public static final ItemStack diamondBlockStack = new ItemStack(Block.blockDiamond);
	public static final ItemStack goldBlockStack = new ItemStack(Block.blockGold);
	public static final ItemStack blackWoolStack = new ItemStack(Block.cloth, 1, 15);
	public static final ItemStack woolStack = new ItemStack(Block.cloth);
	public static final ItemStack uraniumBlockStack = new ItemStack(Blocks.uraniumBlock);
	public static final ItemStack leadStack = new ItemStack(Items.leadIngot);
	public static final ItemStack leadPlateStack = new ItemStack(Items.leadPlate);
	public static final ItemStack barStack = new ItemStack(Block.fenceIron);
	
}
