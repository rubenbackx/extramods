package rubenbackx.extraStuff.recipes;

import java.util.ArrayList;

import ic2.api.recipe.RecipeInputItemStack;
import ic2.api.recipe.Recipes;
import mods.railcraft.api.crafting.RailcraftCraftingManager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import rubenbackx.extraStuff.blocks.BlockInfo;
import rubenbackx.extraStuff.blocks.Blocks;
import rubenbackx.extraStuff.items.Items;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.registry.GameRegistry;

public class RecipesList {
	
	public static void init(){
		
		if(Loader.isModLoaded("IC2")){
			
			try{
				
				ItemStack uraniumDust = ic2.api.item.Items.getItem("crushedUraniumOre");
				ItemStack fertilizer = ic2.api.item.Items.getItem("fertilizer");
				
				GameRegistry.addRecipe(new ItemStack(Blocks.poopBlock, 1), "xx",
																		   "xx",  
				'x', fertilizer);
				
				Recipes.macerator.addRecipe(new RecipeInputItemStack(new ItemStack(Items.uranium)), null, uraniumDust);
				
				System.out.println("Industrial Craft 2 integration for Extra Mods++ was loaded successfully");
				
			}catch(Exception e){
				
				System.out.println("Indusrtial Craft 2 integration for Extra Mods++ was unsuccessful");
				
			}
			
		}else{
			
			System.out.println("Indusrtial Craft 2 integration for Extra Mods++ was unsuccessful because mod is not installed");
			
		}
		
		if(Loader.isModLoaded("Railcraft")){
			
			try{
				
				ArrayList<ItemStack> lead = OreDictionary.getOres("ingotLead");
				
				for(int i = 0; i <= lead.size(); i++){
					RailcraftCraftingManager.rollingMachine.addRecipe(new ItemStack(Items.leadPlate, 4), new Object[]{"xx",
						   																							  "xx",
				    Character.valueOf('x'), lead.get(i)});
				}
				
				System.out.println("Railcraft integration for Extra Mods++ was loaded successfully");
				
			}catch(Exception e){
				
				System.out.println("Railcraft integration for Extra Mods++ was unsuccessful");
				
			}
			
		}else{
			
			System.out.println("Railcraft integration for Extra Mods++ was unsuccessful because mod is not installed");
			
		}
		
		GameRegistry.addRecipe(new ItemStack(Items.wand), " x",
														  "/ ",  
		'x', ItemStacks.starStack, '/', ItemStacks.blazeStack);
		
		GameRegistry.addRecipe(new ItemStack(Blocks.anvilTrap), "oxo",
																"o_o", 
																"o-o", 
		'x', ItemStacks.anvilStack, '_', ItemStacks.plateStack, 'o', ItemStacks.ironStack, '-', ItemStacks.redstoneStack);

		if(BlockInfo.BOMB_ENABLED){
		
		    GameRegistry.addRecipe(new ItemStack(Blocks.worldBomb), "oxo",
				    												"x-x", 
			    													"oxo", 
		    'x', ItemStacks.lapisStack, 'o', ItemStacks.redstoneStack, '-', ItemStacks.wandStack);
		
		}
		
		GameRegistry.addRecipe(new ItemStack(Blocks.poopBlock, 2), "oxo",
																   "o-o", 
																   "ooo", 
		'x', ItemStacks.waterStack, 'o', ItemStacks.dirtStack, '-', ItemStacks.cacoaStack);
		
		GameRegistry.addRecipe(new ItemStack(Blocks.timeBomb, 1), "x-x",
																  "-o-", 
																  "x-x", 
		'x', ItemStacks.TNTStack, 'o', ItemStacks.clockStack, '-', ItemStacks.goldStack);
		
		GameRegistry.addRecipe(new ShapedOreRecipe(Blocks.timeBomb, true, new Object[]{"x-x",
																					   "-o-",
																					   "x-x",
		Character.valueOf('-'), "ingotBronze", Character.valueOf('x'), ItemStacks.TNTStack, Character.valueOf('o'), ItemStacks.clockStack}));
		
		if(BlockInfo.NUKE_ENABLED){
		
		    GameRegistry.addRecipe(new ShapedOreRecipe(Blocks.nuke, true, new Object[]{"xxx",
			    												  					   "xox", 
				    											  					   "x x", 
		    Character.valueOf('x'), "plateLead", Character.valueOf('o'), ItemStacks.uraniumBlockStack}));
		
		}
		
		GameRegistry.addRecipe(new ItemStack(Blocks.blockOfDoom, 1), "ooo",
																	 "d-g", 
																	 "oxo", 
		'o', ItemStacks.ironStack, 'd', ItemStacks.diamondBlockStack, 'g', ItemStacks.goldBlockStack, 'x', ItemStacks.plateStack, '-', ItemStacks.redstoneStack);
		
		GameRegistry.addRecipe(new ItemStack(Blocks.frame, 1), "xxx",
				  											   "ooo", 
				  											   "xxx", 
        'x', ItemStacks.ironStack, 'o', ItemStacks.barStack);
		
		GameRegistry.addRecipe(new ItemStack(Blocks.dice, 1), "xxx",
															  "xox", 
															  "xxx", 
        'x', ItemStacks.woolStack, 'o', ItemStacks.blackWoolStack);
		
		GameRegistry.addRecipe(new ItemStack(Blocks.uraniumBlock, 1), "xxx",
				                                                      "xxx",
																	  "xxx",
		'x', ItemStacks.uraniumStack);
		
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.leadPlate, true, new Object[]{"xx",
                												  					   "xx",
        Character.valueOf('x'), "ingotLead"}));
		
		GameRegistry.addSmelting(Blocks.leadOre.blockID, new ItemStack(Items.leadIngot), 0.2F);
		
	}
	
}
