package rubenbackx.extraStuff.proxy;

import net.minecraftforge.client.MinecraftForgeClient;
import rubenbackx.extraStuff.blocks.BlockInfo;
import rubenbackx.extraStuff.client.RenderCreeperBlock;
import rubenbackx.extraStuff.client.RenderCreeperBlockItem;
import rubenbackx.extraStuff.client.RenderNuclearBomb;
import rubenbackx.extraStuff.client.RenderNuclearBombItem;
import rubenbackx.extraStuff.client.RenderWand;
import rubenbackx.extraStuff.client.RenderWandItem;
import rubenbackx.extraStuff.tileEntities.TileEntityCreeperBlock;
import rubenbackx.extraStuff.tileEntities.TileEntityNuclearBomb;
import rubenbackx.extraStuff.tileEntities.TileEntityWand;
import cpw.mods.fml.client.registry.ClientRegistry;


public class ClientProxy extends CommonProxy{
	
	@Override
	public void initSounds() {
		
	}
	
	@Override
	public void initRenderers() {
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityNuclearBomb.class, new RenderNuclearBomb());
		MinecraftForgeClient.registerItemRenderer(BlockInfo.NUKE_ID, new RenderNuclearBombItem());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWand.class, new RenderWand());
		MinecraftForgeClient.registerItemRenderer(BlockInfo.WAND_BLOCK_ID, new RenderWandItem());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCreeperBlock.class, new RenderCreeperBlock());
		MinecraftForgeClient.registerItemRenderer(BlockInfo.CREEPER_BLOCK_ID, new RenderCreeperBlockItem());
	}

}
