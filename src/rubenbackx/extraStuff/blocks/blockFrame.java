package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockFrame extends Block{

	@SideOnly(Side.CLIENT)
	private Icon topIcon;
	@SideOnly(Side.CLIENT)
	private Icon sideIcon;
	
	public BlockFrame(int id) {
		super(id, Material.iron);
	    setUnlocalizedName(BlockInfo.FRAME_UNAME);
	    setHardness(2.0F);
	    setStepSound(Block.soundMetalFootstep);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		sideIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.FRAME_ICON_SIDE);
		topIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.FRAME_ICON_TOP);
	}
	
	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int nbid){
		
		if(!world.isRemote){
			
			if(world.getBlockId(x+1, y, z) == Block.redstoneLampActive.blockID
			| world.getBlockId(x-1, y, z) == Block.redstoneLampActive.blockID
			| world.getBlockId(x, y-1, z) == Block.redstoneLampActive.blockID
			| world.getBlockId(x, y, z+1) == Block.redstoneLampActive.blockID
			| world.getBlockId(x, y, z-1) == Block.redstoneLampActive.blockID){
				
				if(world.getBlockId(x+1, y, z) == Block.tnt.blockID
				| world.getBlockId(x-1, y, z) == Block.tnt.blockID
				| world.getBlockId(x, y-1, z) == Block.tnt.blockID
				| world.getBlockId(x, y, z+1) == Block.tnt.blockID
				| world.getBlockId(x, y, z-1) == Block.tnt.blockID){
				
					if(!world.isRaining()){
					
						world.setBlockMetadataWithNotify(x, y, z, 1, 3);
				
					}
					
				}	
				
			}
			
		}
		
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int side, int meta){
		
		switch(side){
			case 1:
				return topIcon;
			case 0:
				return topIcon;
			default:
				return sideIcon;
		}
		
	}
	
	@Override
    @SideOnly(Side.CLIENT)
    public int getRenderBlockPass()
    {
        return 0;
    }
    
    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }
    
    

}
