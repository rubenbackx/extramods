package rubenbackx.extraStuff.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.util.Icon;

public class BlockUranium extends StandardBlock implements Icon {

	public BlockUranium(int id) {
		super(id, Material.iron);
	    setUnlocalizedName(BlockInfo.URANIUM_UNAME);
	    setHardness(2.0F);
	    setStepSound(soundMetalFootstep);
	}

	@Override
	public int getIconWidth() {
		return 32;
	}

	@Override
	public int getIconHeight() {
		return 32;
	}

	@Override
	public float getMinU() {
		return 0;
	}

	@Override
	public float getMaxU() {
		return 0;
	}

	@Override
	public float getInterpolatedU(double d0) {
		return 0;
	}

	@Override
	public float getMinV() {
		return 0;
	}

	@Override
	public float getMaxV() {
		return 0;
	}

	@Override
	public float getInterpolatedV(double d0) {
		return 0;
	}

	@Override
	public String getIconName() {
		return null;
	}

}
