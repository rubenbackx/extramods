package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import rubenbackx.extraStuff.client.Particles;
import rubenbackx.extraStuff.items.Items;
import rubenbackx.extraStuff.lib.Modstuff;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockUraniumOre extends Block{

	@SideOnly(Side.CLIENT)	
	public static Icon particleIcon;
	
	public BlockUraniumOre(int id) {
	    super(id, Material.iron);
	    setUnlocalizedName(BlockInfo.URANIUM_ORE_UNAME);
	    setHardness(3.8F);
	    setStepSound(Block.soundStoneFootstep);
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		blockIcon = register.registerIcon(Modstuff.MOD_ID + ":" + BlockInfo.URANIUM_ORE_ICON);
		particleIcon = register.registerIcon(Modstuff.MOD_ID + ":uranium");
		
	}
	
	@Override
	public int idDropped(int id, Random random, int meta){
		
		return Items.uranium.itemID;	
		
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random rand){
		
		float particleX = x + rand.nextFloat();
		float particleY = y + rand.nextFloat();
		float particleZ = z + rand.nextFloat();
		
		float particleMotionX = -0.2F + rand.nextFloat();
		float particleMotionY = -0.2F + rand.nextFloat();
		float particleMotionZ = -0.2F + rand.nextFloat();
		
		Particles.URANIUM.spawnParticle(world, particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
		Particles.URANIUM.spawnParticle(world, particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
		Particles.URANIUM.spawnParticle(world, particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
		Particles.URANIUM.spawnParticle(world, particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
		Particles.URANIUM.spawnParticle(world, particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
		Particles.URANIUM.spawnParticle(world, particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
		Particles.URANIUM.spawnParticle(world, particleX, particleY, particleZ, particleMotionX, particleMotionY, particleMotionZ);
		
	}

}
