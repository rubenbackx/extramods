package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockDice extends Block{

	@SideOnly(Side.CLIENT)
	private Icon icon1;
	@SideOnly(Side.CLIENT)
	private Icon icon2;
	@SideOnly(Side.CLIENT)
	private Icon icon3;
	@SideOnly(Side.CLIENT)
	private Icon icon4;
	@SideOnly(Side.CLIENT)
	private Icon icon5;
	@SideOnly(Side.CLIENT)
	private Icon icon6;
	
	public BlockDice(int id) {
		super(id, Material.cloth);
	    setUnlocalizedName(BlockInfo.DICE_UNAME);
	    setHardness(1.0F);
	    setStepSound(Block.soundClothFootstep);
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int id, float f1, float f2, float f3){
		
		Random rand = new Random();
		int number = rand.nextInt(6) + 1;
		
		while(number == world.getBlockMetadata(x, y, z)){
			
			number = rand.nextInt(6) + 1;
			
		}
		
		world.setBlockMetadataWithNotify(x, y, z, number, 3);
		
		return true;
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register){
		
		icon1 = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DICE_ICON_1);
		icon2 = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DICE_ICON_2);
		icon3 = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DICE_ICON_3);
		icon4 = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DICE_ICON_4);
		icon5 = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DICE_ICON_5);
		icon6 = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DICE_ICON_6);
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public Icon getIcon(int side, int meta){
		
		if(side == 1){
			switch(meta){
				case 1:
					return icon1;
				case 2:
					return icon2;
				case 3:
					return icon3;
				case 4:
					return icon4;
				case 5:
					return icon5;
				case 6:
					return icon6;
				default:
					return icon1;
			}
		}else if(side == 0){
			switch(meta){
				case 1:
					return icon6;
				case 2:
					return icon5;
				case 3:
					return icon4;
				case 4:
					return icon3;
				case 5:
					return icon2;
				case 6:
					return icon1;
				default:
					return icon6;
			}
		}else if(side == 2){
			switch(meta){
				case 1:
					return icon3;
				case 2:
					return icon4;
				case 3:
					return icon5;
				case 4:
					return icon6;
				case 5:
					return icon1;
				case 6:
					return icon2;
				default:
					return icon3;
			}
		}else if(side == 3){
			switch(meta){
				case 1:
					return icon4;
				case 2:
					return icon3;
				case 3:
					return icon2;
				case 4:
					return icon1;
				case 5:
					return icon6;
				case 6:
					return icon5;
				default:
					return icon4;
			}
		}else if(side == 4){
			switch(meta){
				case 1:
					return icon5;
				case 2:
					return icon6;
				case 3:
					return icon1;
				case 4:
					return icon2;
				case 5:
					return icon3;
				case 6:
					return icon4;
				default:
					return icon5;
			}
		}else if(side == 5){
			switch(meta){
				case 1:
					return icon2;
				case 2:
					return icon1;
				case 3:
					return icon6;
				case 4:
					return icon5;
				case 5:
					return icon4;
				case 6:
					return icon3;
				default:
					return icon2;
			}
		}else{
			return icon1;
		}
	}
	
}
