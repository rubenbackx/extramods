package rubenbackx.extraStuff.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.oredict.OreDictionary;
import rubenbackx.extraStuff.tileEntities.TileEntityAnvilTrap;
import rubenbackx.extraStuff.tileEntities.TileEntityCreeperBlock;
import rubenbackx.extraStuff.tileEntities.TileEntityNuclearBomb;
import rubenbackx.extraStuff.tileEntities.TileEntityTimeBomb;
import rubenbackx.extraStuff.tileEntities.TileEntityWand;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class Blocks {
	
	public static Block anvilTrap;
	public static Block worldBomb;
	public static Block poopBlock;
	public static Block timeBomb;
	public static Block nuke;
	public static Block uraniumOre;
	public static Block blockOfDoom;
	public static Block dice;
	public static Block wand;
	public static Block creeper;
	public static Block frame;
	public static Block uraniumBlock;
	public static Block leadOre;

	public static void init(){

		anvilTrap = new BlockAnvilTrap(BlockInfo.TRAP_ID);
		worldBomb = new BlockWorldBomb(BlockInfo.BOMB_ID);
		poopBlock = new StandardBlock(BlockInfo.POOP_ID, Material.grass).setUnlocalizedName(BlockInfo.POOP_UNAME).setHardness(1.0F);
		timeBomb = new BlockTimeBomb(BlockInfo.TIMEBOMB_ID);
		nuke = new BlockNuclearBomb(BlockInfo.NUKE_ID);
		uraniumOre = new BlockUraniumOre(BlockInfo.URANIUM_ORE_ID);
		blockOfDoom = new BlockOfDoom(BlockInfo.DOOM_ID);
		dice = new BlockDice(BlockInfo.DICE_ID);
		wand = new BlockWand(BlockInfo.WAND_BLOCK_ID);
		creeper = new BlockCreeper(BlockInfo.CREEPER_BLOCK_ID);
		frame = new BlockFrame(BlockInfo.FRAME_ID);
		uraniumBlock = new BlockUranium(BlockInfo.URANIUM_BLOCK_ID);
		leadOre = new StandardBlock(BlockInfo.LEAD_ORE_ID, Material.iron).setUnlocalizedName(BlockInfo.LEAD_ORE_UNAME).setHardness(2.0F).setResistance(10.0F);
		
		LanguageRegistry.addName(anvilTrap, BlockInfo.TRAP_NAME);
		GameRegistry.registerBlock(anvilTrap, BlockInfo.TRAP_UNAME);
		LanguageRegistry.addName(worldBomb, BlockInfo.BOMB_NAME);
		GameRegistry.registerBlock(worldBomb, BlockInfo.BOMB_UNAME);
		LanguageRegistry.addName(poopBlock, BlockInfo.POOP_NAME);
		GameRegistry.registerBlock(poopBlock, BlockInfo.POOP_UNAME);
		//TODO Implement sound again
		addWipName(timeBomb, BlockInfo.TIMEBOMB_NAME);
		GameRegistry.registerBlock(timeBomb, BlockInfo.TIMEBOMB_UNAME);
		LanguageRegistry.addName(nuke, BlockInfo.NUKE_NAME);
		GameRegistry.registerBlock(nuke, BlockInfo.NUKE_UNAME);
		LanguageRegistry.addName(uraniumOre, BlockInfo.URANIUM_ORE_NAME);
		GameRegistry.registerBlock(uraniumOre, BlockInfo.URANIUM_ORE_UNAME);
		//TODO Make internal tank
		//the tank has to wait until I understand the fluid system
		addWipName(blockOfDoom, BlockInfo.DOOM_NAME);
		GameRegistry.registerBlock(blockOfDoom, BlockInfo.DOOM_UNAME);
		LanguageRegistry.addName(dice, BlockInfo.DICE_NAME);
		GameRegistry.registerBlock(dice, BlockInfo.DICE_UNAME);
		//TODO Add more items
		addWipName(wand, BlockInfo.WAND_NAME);
		GameRegistry.registerBlock(wand, BlockInfo.WAND_UNAME);
		//TODO Rotation
		addWipName(creeper, BlockInfo.CREEPER_NAME);
		GameRegistry.registerBlock(creeper, BlockInfo.CREEPER_UNAME);
		LanguageRegistry.addName(frame, BlockInfo.FRAME_NAME);
		GameRegistry.registerBlock(frame, BlockInfo.FRAME_UNAME);
		LanguageRegistry.addName(uraniumBlock, BlockInfo.URANIUM_NAME);
		GameRegistry.registerBlock(uraniumBlock, BlockInfo.URANIUM_UNAME);
		LanguageRegistry.addName(leadOre, BlockInfo.LEAD_ORE_NAME);
		GameRegistry.registerBlock(leadOre, BlockInfo.LEAD_ORE_UNAME);
				
		MinecraftForge.setBlockHarvestLevel(anvilTrap, "pickaxe", 0);
		MinecraftForge.setBlockHarvestLevel(blockOfDoom, "pickaxe", 0);
		MinecraftForge.setBlockHarvestLevel(frame, "pickaxe", 0);
		MinecraftForge.setBlockHarvestLevel(uraniumOre, "pickaxe", 2);
		MinecraftForge.setBlockHarvestLevel(poopBlock, "shovel", 0);
		MinecraftForge.setBlockHarvestLevel(uraniumBlock, "pickaxe", 0);	
		MinecraftForge.setBlockHarvestLevel(leadOre, "pickaxe", 2);
		
		GameRegistry.registerTileEntity(TileEntityTimeBomb.class, "tileBomb");
		GameRegistry.registerTileEntity(TileEntityNuclearBomb.class, "tileNuke");
		GameRegistry.registerTileEntity(TileEntityWand.class, "tileWand");
		GameRegistry.registerTileEntity(TileEntityCreeperBlock.class, "tileCreeper");
		GameRegistry.registerTileEntity(TileEntityAnvilTrap.class, "tileTrap");
		
		OreDictionary.registerOre("oreUranium", new ItemStack(uraniumOre));
		OreDictionary.registerOre("oreLead", new ItemStack(leadOre));
		
	}
	
	private static void addWipName(Block block, String name){
		LanguageRegistry.addName(block, name + " [WIP]");
	}
	
}
