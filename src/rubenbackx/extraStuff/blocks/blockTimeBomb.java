package rubenbackx.extraStuff.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import rubenbackx.extraStuff.tileEntities.TileEntityTimeBomb;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTimeBomb extends BlockContainer implements Icon{

	@SideOnly(Side.CLIENT)
	private Icon topIcon;
	@SideOnly(Side.CLIENT)
	private Icon topIcon_red;
	@SideOnly(Side.CLIENT)
	private Icon[] sideIcons;
	@SideOnly(Side.CLIENT)
	private Icon bottomIcon;
	@SideOnly(Side.CLIENT)
	private Icon bottomIcon_red;
	
	protected BlockTimeBomb(int id) {
	    super(id, Material.tnt);
	    setUnlocalizedName(BlockInfo.TIMEBOMB_UNAME);
	    setHardness(1.0F);
	    setStepSound(soundPowderFootstep);
	}

	@Override
	public void registerIcons(IconRegister register){
		
		topIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.TIMEBOMB_ICON_TOP);
		topIcon_red = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.TIMEBOMB_ICON_TOP_RED);
		sideIcons = new Icon[BlockInfo.TIMEBOMB_ICON_SIDES.length];
		for(int i = 0; i < BlockInfo.TIMEBOMB_ICON_SIDES.length; i++){
			sideIcons[i] = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.TIMEBOMB_ICON_SIDES[i]);
		}
		bottomIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.TIMEBOMB_ICON_BOTTOM);
		bottomIcon_red = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.TIMEBOMB_ICON_BOTTOM_RED);
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public Icon getIcon(int side, int meta){
		
		if(side==0){
			
			if(meta <= 5 && meta != 0){
				return bottomIcon_red;	
			}else{
				return bottomIcon;
			}
				
		}else if(side==1){
			
			if(meta <= 5 && meta != 0){
				return topIcon_red;	
			}else{
				return topIcon;
			}

		}else{
			
			switch(meta){
				case 0:
					return sideIcons[14];
				case 1:
					return sideIcons[0];
				case 2:
					return sideIcons[1];
				case 3:
					return sideIcons[2];
				case 4:
					return sideIcons[3];
				case 5:
					return sideIcons[4];
				case 6:
					return sideIcons[5];
				case 7:
					return sideIcons[6];
				case 8:
					return sideIcons[7];
				case 9:
					return sideIcons[8];
				case 10:
					return sideIcons[9];
				case 11:
					return sideIcons[10];
				case 12:
					return sideIcons[11];
				case 13:
					return sideIcons[12];
				case 14:
					return sideIcons[13];
				case 15:
					return sideIcons[14];
				default:
					return sideIcons[14];
					
			}
			
		}
		
	}
	
	@Override
    public TileEntity createNewTileEntity(World world) {
	    return new TileEntityTimeBomb();
    }

	@Override
	public int getIconWidth() {
		return 32;
	}

	@Override
	public int getIconHeight() {
		return 32;
	}

	@Override
	public float getMinU() {
		return 0;
	}

	@Override
	public float getMaxU() {
		return 0;
	}

	@Override
	public float getInterpolatedU(double d0) {
		return 0;
	}

	@Override
	public float getMinV() {
		return 0;
	}

	@Override
	public float getMaxV() {
		return 0;
	}

	@Override
	public float getInterpolatedV(double d0) {
		return 0;
	}

	@Override
	public String getIconName() {
		return null;
	}

}
