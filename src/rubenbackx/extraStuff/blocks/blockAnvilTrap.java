package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import rubenbackx.extraStuff.ExtraStuff;
import rubenbackx.extraStuff.tileEntities.TileEntityAnvilTrap;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockAnvilTrap extends BlockContainer{

	public BlockAnvilTrap(int par1) {
	    super(par1, Material.iron);
	    setUnlocalizedName(BlockInfo.TRAP_UNAME);
	    setHardness(2.0F);
	    setStepSound(soundAnvilFootstep);
	}
	
	@SideOnly(Side.CLIENT)
	private Icon topIcon;
	@SideOnly(Side.CLIENT)
	private Icon sideIcon;
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register){
		
		topIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.TRAP_ICON_TOP);
		sideIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.TRAP_ICON_SIDE);
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public Icon getIcon(int side, int meta){
		
		if(side==1){
			return topIcon;
		}else{
			return sideIcon;
		}
		
	}
	
	 @Override
     public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int metadata, float what, float these, float are) {
             TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
             if (tileEntity == null || player.isSneaking()) {
                     return false;
             }
     //code to open gui explained later
     player.openGui(ExtraStuff.instance, 0, world, x, y, z);
             return true;
     }

     @Override
     public void breakBlock(World world, int x, int y, int z, int par5, int par6) {
             dropItems(world, x, y, z);
             super.breakBlock(world, x, y, z, par5, par6);
     }

     private void dropItems(World world, int x, int y, int z){
             Random rand = new Random();

             TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
             if (!(tileEntity instanceof IInventory)) {
                     return;
             }
             IInventory inventory = (IInventory) tileEntity;

             for (int i = 0; i < inventory.getSizeInventory(); i++) {
                     ItemStack item = inventory.getStackInSlot(i);

                     if (item != null && item.stackSize > 0) {
                             float rx = rand.nextFloat() * 0.8F + 0.1F;
                             float ry = rand.nextFloat() * 0.8F + 0.1F;
                             float rz = rand.nextFloat() * 0.8F + 0.1F;

                             EntityItem entityItem = new EntityItem(world,
                                             x + rx, y + ry, z + rz,
                                             new ItemStack(item.itemID, item.stackSize, item.getItemDamage()));

                             if (item.hasTagCompound()) {
                                     entityItem.getEntityItem().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
                             }

                             float factor = 0.05F;
                             entityItem.motionX = rand.nextGaussian() * factor;
                             entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
                             entityItem.motionZ = rand.nextGaussian() * factor;
                             world.spawnEntityInWorld(entityItem);
                             item.stackSize = 0;
                     }
             }
     }
	
	@Override
	public void onEntityWalking(World world, int x, int y, int z, Entity entity){
		
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		IInventory inv = (IInventory) tileEntity;
		ItemStack item = inv.getStackInSlot(0);
		
		if(!world.isRemote){
			
			if(item != null && item.stackSize >= 25){
				
				item.stackSize = item.stackSize - 25;
				
				for(int i=-2; i <= 2; i++){
					for(int j=-2; j <= 2; j++){
						
						setAir(world, x + i, y, z + j);
						spawnAnvil(world, x + i, y + 10, z + j);
						
					}
				}
				
			}
			
		}
		
	}
	
	private void spawnAnvil(World world, int x, int y, int z){
		
		if(world.isAirBlock(x, y, z)){
			world.setBlock(x, y, z, Block.anvil.blockID);
		}
		
	}
	
	private void setAir(World world, int x, int y, int z){
		
		world.setBlockToAir(x, y, z);

	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityAnvilTrap();
	}

}
