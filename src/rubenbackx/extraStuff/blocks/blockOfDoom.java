package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockOfDoom extends Block{

	public BlockOfDoom(int id) {
		super(id, Material.iron);
	    setUnlocalizedName(BlockInfo.DOOM_UNAME);
	    setHardness(2.0F);
	    setStepSound(soundMetalFootstep);
	}
	
	@SideOnly(Side.CLIENT)
	private Icon topIcon;
	@SideOnly(Side.CLIENT)
	private Icon sideIcon;

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		topIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DOOM_ICON_TOP);
		sideIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.DOOM_ICON_SIDE);
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public Icon getIcon(int side, int meta){
		
		if(side==1){
			return topIcon;
		}else{
			return sideIcon;
		}
		
	}
	
	@Override
	public void onEntityWalking(World world, int x, int y, int z, Entity entity){
		
		Random rand = new Random();
		int randomNum = rand.nextInt(100) + 1;
		
		//randomNum = 1;
		//System.out.println(randomNum);
		
		if(!world.isRemote){
			
			if(randomNum >= 3){
				for(int i=-1; i <= 1; i++){
					for(int j=-1; j <= 1; j++){
						setLava(world, x + i, y, z + j);
					}
				}
			}else{
				spawnItems(world, x, y, z, (new ItemStack(Item.ingotGold, 32)));
				spawnItems(world, x, y, z, (new ItemStack(Item.diamond, 64)));
				world.setBlockToAir(x, y, z);
			}
		}
		
	}
	
	private void spawnItems(World world, int x, int y, int z, ItemStack itemstack){
		
		world.spawnEntityInWorld(new EntityItem(world, x, y + 100, z, itemstack));
		
	}
	
	private void setLava(World world, int x, int y, int z){
		
		world.setBlock(x, y, z, Block.lavaMoving.blockID);

	}
	
}
