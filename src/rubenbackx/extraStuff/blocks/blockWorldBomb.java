package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import rubenbackx.extraStuff.items.Items;
import rubenbackx.extraStuff.tileEntities.TileEntityWorldBomb;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockWorldBomb extends BlockContainer{

	public BlockWorldBomb(int par1) {
	    super(par1, Material.iron);
	    setUnlocalizedName(BlockInfo.BOMB_UNAME);
	    setHardness(0.01F);
	    setStepSound(soundStoneFootstep);
	    setLightValue(0.2F);
	}
	
	@Override
	public int quantityDropped(Random random){
		
		return 0;
		
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int id, float f1, float f2, float f3){

		if(!world.isRemote){
			
			if(player.isSneaking() && world.getBlockMetadata(x, y, z) == 1){
				
				world.setBlockToAir(x, y, z);
				world.spawnEntityInWorld(new EntityItem(world, x, y, z, new ItemStack(Blocks.worldBomb)));
			
			}else if(!player.isSneaking()){
				
				world.setBlockMetadataWithNotify(x, y, z, 0, 3);
				
			}
			
			return true;
			
		}else{
			
			return false;
			
		}
	
	}
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z){
		
		world.setBlockMetadataWithNotify(x, y, z, 1, 3);
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register){
		
		blockIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.BOMB_ICON);
		
	}	

	@Override
    public TileEntity createNewTileEntity(World world) {
	    return new TileEntityWorldBomb();
    }

}
