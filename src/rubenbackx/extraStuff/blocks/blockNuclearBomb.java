package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import rubenbackx.extraStuff.items.Items;
import rubenbackx.extraStuff.lib.Modstuff;
import rubenbackx.extraStuff.tileEntities.TileEntityNuclearBomb;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockNuclearBomb extends BlockContainer{
	
	public BlockNuclearBomb(int id) {
		super(id, Material.iron);
	    setUnlocalizedName(BlockInfo.NUKE_UNAME);
	    setHardness(5.0F);
	    setStepSound(Block.soundMetalFootstep);
	    setResistance(10000F);
    }

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int nbid){
		
		Random rand = new Random();
		double randomNumber;
		double randomY;
		
		System.out.println(world.getBlockMetadata(x, y-1, z));
		
		if(!world.isRemote){
			
			if(world.getBlockId(x, y-1, z) == Blocks.frame.blockID){
				
				if(world.getBlockMetadata(x, y-1, z) == 1){
					
					world.setBlockToAir(x, y, z);
					
					for(int i = -2; i <= 2; i++){
						for(int j = -2; j <= 2; j++){
							randomNumber = rand.nextInt(10) + 1;
							randomY = rand.nextInt(2) + 1;
							world.createExplosion(null, x+0.5+i+randomNumber, y+2+randomY+((i/10)+(j/10)), z+0.5+j+randomNumber, 25, true);
						}
					}
					
				}
				
			}else{
				
				world.setBlockToAir(x, y, z);
				world.spawnEntityInWorld(new EntityItem(world, x, y, z, (new ItemStack(Blocks.nuke))));
				
			}
			
		}
		
	}
	
	@Override
	public void onBlockAdded(World world, int x, int y, int z){
		
		if(!world.isRemote){
			
			if(world.getBlockId(x, y-1, z) != Blocks.frame.blockID){
				world.setBlockToAir(x, y, z);
				world.spawnEntityInWorld(new EntityItem(world, x, y, z, (new ItemStack(Blocks.nuke))));
				
			}
			
		}
		
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		blockIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.NUKE_ICON);
				
	}
	
    @Override
    public int getRenderType() {
            return -1;
    }
    
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    @Override
    public boolean renderAsNormalBlock() {
            return false;
    }
	
	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityNuclearBomb();
	}
	
}
