package rubenbackx.extraStuff.blocks;

import net.minecraftforge.common.Property;

public class BlockInfo {
	
	public static final String TEXTURE_LOCATION = "extraStuff";
	
	public static boolean BOMB_ENABLED;
	public static boolean NUKE_ENABLED;
	
	public static int TRAP_ID;
	public static final String TRAP_KEY = "Anvil trap";
	public static final int TRAP_DEFAULT = 2141;
	public static final String TRAP_UNAME = "anviltrap";
	public static final String TRAP_NAME = "Anvil Trap";
	public static final String TRAP_ICON_SIDE = "anvilblock_side";
	public static final String TRAP_ICON_TOP = "anvilblock_top";
	
	public static int BOMB_ID;
	public static final String BOMB_KEY = "World bomb";
	public static final int BOMB_DEFAULT = 2142;
	public static final String BOMB_UNAME = "worldbomb";
	public static final String BOMB_NAME = "World Bomb";
	public static final String BOMB_ICON = "worldBomb";
	
	public static int POOP_ID;
	public static final String POOP_KEY = "Poop";
	public static final int POOP_DEFAULT = 2143;
	public static final String POOP_UNAME = "poop";
	public static final String POOP_NAME = "Poop Block";
	
	public static int TIMEBOMB_ID;
	public static final String TIMEBOMB_KEY = "Time bomb";
	public static final int TIMEBOMB_DEFAULT = 2144;
	public static final String TIMEBOMB_UNAME = "timebomb";
	public static final String TIMEBOMB_NAME = "Time Bomb";
	public static final String[] TIMEBOMB_ICON_SIDES = {"timeBomb_side_1", "timeBomb_side_2", "timeBomb_side_3", "timeBomb_side_4", "timeBomb_side_5", "timeBomb_side_6", "timeBomb_side_7", "timeBomb_side_8", "timeBomb_side_9", "timeBomb_side_10", "timeBomb_side_11", "timeBomb_side_12", "timeBomb_side_13", "timeBomb_side_14", "timeBomb_side_15"};
	public static final String TIMEBOMB_ICON_TOP = "timeBomb_top";
	public static final String TIMEBOMB_ICON_BOTTOM = "timeBomb_bottom";
	public static final String TIMEBOMB_ICON_TOP_RED = "timeBomb_top_red";
	public static final String TIMEBOMB_ICON_BOTTOM_RED = "timeBomb_bottom_red";
	
	public static int NUKE_ID;
	public static final String NUKE_KEY = "Nuclear bomb";
	public static final int NUKE_DEFAULT = 2145;
	public static final String NUKE_UNAME = "nuke";
	public static final String NUKE_NAME = "Nuclear Bomb";
	public static final String NUKE_ICON = "nuke";
	
	public static int URANIUM_ORE_ID;
	public static final String URANIUM_ORE_KEY = "Uranium ore";
	public static final int URANIUM_ORE_DEFAULT = 2146;
	public static final String URANIUM_ORE_UNAME = "uraniumore";
	public static final String URANIUM_ORE_NAME = "Uranium Ore";
	public static final String URANIUM_ORE_ICON = "uraniumOre";
	
	public static int DOOM_ID;
	public static final String DOOM_KEY = "Block of doom";
	public static final int DOOM_DEFAULT = 2147;
	public static final String DOOM_UNAME = "doom";
	public static final String DOOM_NAME = "Block of Doom";
	public static final String DOOM_ICON_SIDE = "doomBlock_side";
	public static final String DOOM_ICON_TOP = "doomBlock_top";
	
	public static int DICE_ID;
	public static final String DICE_KEY = "Dice";
	public static final int DICE_DEFAULT = 2148;
	public static final String DICE_UNAME = "dice";
	public static final String DICE_NAME = "Dice";
	public static final String DICE_ICON_1 = "dice1";
	public static final String DICE_ICON_2 = "dice2";
	public static final String DICE_ICON_3 = "dice3";
	public static final String DICE_ICON_4 = "dice4";
	public static final String DICE_ICON_5 = "dice5";
	public static final String DICE_ICON_6 = "dice6";
	
	public static int WAND_BLOCK_ID;
	public static final String WAND_KEY = "Wand block";
	public static final int WAND_DEFAULT = 2149;
	public static final String WAND_UNAME = "wandblock";
	public static final String WAND_NAME = "Wand";
	public static final String WAND_ICON = "wandBlock";
	
	public static int CREEPER_BLOCK_ID;
	public static final String CREEPER_KEY = "Creeper block";
	public static final int CREEPER_DEFAULT = 2150;
	public static final String CREEPER_UNAME = "creeperblock";
	public static final String CREEPER_NAME = "Creeper Block";
	public static final String CREEPER_ICON = "creeperIcon";
	
	public static int FRAME_ID;
	public static final String FRAME_KEY = "Frame";
	public static final int FRAME_DEFAULT = 2151;
	public static final String FRAME_UNAME = "frame";
	public static final String FRAME_NAME = "Frame";
	public static final String FRAME_ICON_TOP = "frame_top";
	public static final String FRAME_ICON_SIDE = "frame_side";
	
	public static int URANIUM_BLOCK_ID;
	public static final String URANIUM_KEY = "Uranium block";
	public static final int URANIUM_DEFAULT = 2152;
	public static final String URANIUM_UNAME = "uraniumblock";
	public static final String URANIUM_NAME = "Uranium Block";
	
	public static int LEAD_ORE_ID;
	public static final String LEAD_ORE_KEY = "Lead ore";
	public static final int LEAD_ORE_DEFAULT = 2153;
	public static final String LEAD_ORE_UNAME = "leadore";
	public static final String LEAD_ORE_NAME = "Lead Ore";
		
}
