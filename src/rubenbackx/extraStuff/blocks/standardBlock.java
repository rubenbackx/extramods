package rubenbackx.extraStuff.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;

public class StandardBlock extends Block{

	public StandardBlock(int id, Material material) {
	    super(id, material);
    }
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		String name = this.getUnlocalizedName();
		String texturename = name.replace("tile.", "");
		
		blockIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + texturename);
		
	}
	
}
