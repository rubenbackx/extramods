package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import rubenbackx.extraStuff.items.Items;
import rubenbackx.extraStuff.tileEntities.TileEntityWand;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockWand extends BlockContainer{
	
	public BlockWand(int id) {
		super(id, Material.iron);
	    setUnlocalizedName(BlockInfo.WAND_UNAME);
	    setBlockUnbreakable();
	    setStepSound(Block.soundMetalFootstep);
	    setResistance(10000F);
    }
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register){
		blockIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.WAND_ICON);
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int id, float f1, float f2, float f3){

		Random rand = new Random();
		int randomNum = 0;
		
		int currentItem = 0;
		int lastTnt = 999;
		int lastWater = 999;
		int lastLava = 999;
		
		if(!world.isRemote){
			if(player.isSneaking()){
				
				ItemStack stack = new ItemStack(Items.wand, 1, 9999);
				
				stack.stackTagCompound = new NBTTagCompound();
				
				stack.stackTagCompound.setString("owner", player.username);
				
				world.setBlockToAir(x, y, z);
				world.spawnEntityInWorld(new EntityItem(world, x, y, z, stack));
			}else{	
				
				if(player.inventory.mainInventory[player.inventory.currentItem] != null){
				
					if(player.inventory.mainInventory[player.inventory.currentItem].itemID == Block.tnt.blockID){
						
						for(int i = 0; i <= 35; i++){
								
							if(player.inventory.mainInventory[i] != null){
									
								currentItem = player.inventory.mainInventory[i].itemID;
								//System.out.println(currentItem + " " + i);
										
								if(currentItem == Block.tnt.blockID){
											
									if(i == player.inventory.currentItem){
										lastTnt = i;
									}
										
								}
								
							}
								
						}
								
						randomNum = rand.nextInt(8) + 1;
						
						if(lastTnt != 999 && lastTnt == player.inventory.currentItem){
							for(int j = 0; j < player.inventory.mainInventory[lastTnt].stackSize; j++){
								if(randomNum == 1){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x+j, y+2.5, z+j, player));
								}else if(randomNum == 2){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x-j, y+2.5, z-j, player));
								}else if(randomNum == 3){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x-j, y+2.5, z+j, player));
								}else if(randomNum == 4){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x+j, y+2.5, z-j, player));
								}else if(randomNum == 5){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x, y+2.5, z+j, player));
								}else if(randomNum == 6){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x, y+2.5, z-j, player));
								}else if(randomNum == 7){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x+j, y+2.5, z, player));
								}else if(randomNum == 8){
									world.spawnEntityInWorld(new EntityTNTPrimed(world, x-j, y+2.5, z, player));
								}
							}
									
							player.inventory.mainInventory[lastTnt].stackSize = 0;
							
						}
									
					}
				}
						
				return true;
			
			}
			
		}
		
		return false;
	
	}
	
	@Override
	public int idDropped(int id, Random random, int meta){
		
		return Items.wand.itemID;
		
	}
	
    @Override
    public int getRenderType() {
            return -1;
    }
    
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    public boolean renderAsNormalBlock() {
            return false;
    }
	
	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityWand();
	}
	
}
