package rubenbackx.extraStuff.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import rubenbackx.extraStuff.items.Items;
import rubenbackx.extraStuff.tileEntities.TileEntityCreeperBlock;
import rubenbackx.extraStuff.tileEntities.TileEntityWand;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockCreeper extends BlockContainer{
	
	public BlockCreeper(int id) {
		super(id, Material.cloth);
	    setUnlocalizedName(BlockInfo.CREEPER_UNAME);
	    setHardness(2.0F);
	    setStepSound(Block.soundClothFootstep);
    }
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register){
		blockIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.CREEPER_ICON);
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
		
		int rotation = (int)entity.rotationYawHead;
		
		if(rotation >= 315 && rotation < 45){
			
			world.setBlockMetadataWithNotify(x, y, z, 1, 3);
			
		}else if(rotation >= 45 && rotation < 135){
			
			world.setBlockMetadataWithNotify(x, y, z, 2, 3);			
		
		}else if(rotation >= 135 && rotation < 225){

			world.setBlockMetadataWithNotify(x, y, z, 3, 3);
		
		}else if(rotation >= 225 && rotation < 315){

			world.setBlockMetadataWithNotify(x, y, z, 4, 3);			
			
		}
		
	}

	
    @Override
    public int getRenderType() {
            return -1;
    }
    
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    public boolean renderAsNormalBlock() {
            return false;
    }
	
	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityCreeperBlock();
	}
	
}
