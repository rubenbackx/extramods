package rubenbackx.extraStuff.creative;

import rubenbackx.extraStuff.blocks.Blocks;
import rubenbackx.extraStuff.items.Items;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class Creativetabs {
	
	public static CreativeTabs tabExtraStuff;
	
	public static void init(){
		
        tabExtraStuff = new CreativeTabs("tabExtraStuff") {
            public ItemStack getIconItemStack() {
                    return new ItemStack(Blocks.worldBomb, 1, 0);
            }
        };
		
		LanguageRegistry.instance().addStringLocalization("itemGroup.tabExtraStuff", "en_US", "Extra Stuff++");
		
		Blocks.anvilTrap.setCreativeTab(tabExtraStuff);
		Blocks.blockOfDoom.setCreativeTab(tabExtraStuff);
		Blocks.dice.setCreativeTab(tabExtraStuff);
		Blocks.nuke.setCreativeTab(tabExtraStuff);
		Blocks.timeBomb.setCreativeTab(tabExtraStuff);
		Blocks.uraniumOre.setCreativeTab(tabExtraStuff);
		Blocks.worldBomb.setCreativeTab(tabExtraStuff);
		Blocks.creeper.setCreativeTab(tabExtraStuff);
		Blocks.frame.setCreativeTab(tabExtraStuff);
		Blocks.poopBlock.setCreativeTab(tabExtraStuff);
		Blocks.uraniumBlock.setCreativeTab(tabExtraStuff);
		Blocks.leadOre.setCreativeTab(tabExtraStuff);
		
		Items.wand.setCreativeTab(tabExtraStuff);
		Items.uranium.setCreativeTab(tabExtraStuff);
		Items.leadIngot.setCreativeTab(tabExtraStuff);
		Items.leadPlate.setCreativeTab(tabExtraStuff);
		
	}

}
