package rubenbackx.extraStuff;

import rubenbackx.extraStuff.blocks.Blocks;
import rubenbackx.extraStuff.config.ConfigHandler;
import rubenbackx.extraStuff.creative.Creativetabs;
import rubenbackx.extraStuff.inventory.GuiHandler;
import rubenbackx.extraStuff.items.Items;
import rubenbackx.extraStuff.lib.Modstuff;
import rubenbackx.extraStuff.proxy.CommonProxy;
import rubenbackx.extraStuff.recipes.RecipesList;
import rubenbackx.extraStuff.serverSide.PacketHandler;
import rubenbackx.extraStuff.worldGen.WorldGenerationHandlerOres;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;

@Mod(modid = Modstuff.MOD_ID, name = Modstuff.MOD_NAME, version = Modstuff.MOD_VERSION)
@NetworkMod(channels = {Modstuff.MOD_CHANNEL}, clientSideRequired = Modstuff.MOD_CLIENTSIDEREQ, 
            serverSideRequired = Modstuff.MOD_SERVERSIDEREQ, packetHandler = PacketHandler.class)

public class ExtraStuff {

	@Instance(Modstuff.MOD_ID)
	public static ExtraStuff instance;
	
	@SidedProxy(clientSide = "rubenbackx.extraStuff.proxy.ClientProxy", serverSide = "rubenbackx.extraStuff.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) { 
		ConfigHandler.init(event.getSuggestedConfigurationFile());
		
		proxy.initSounds();
		proxy.initRenderers();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {

		Items.init();
		Blocks.init();
		RecipesList.init();
		Creativetabs.init();
		
		new WorldGenerationHandlerOres();
		NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
	
}
