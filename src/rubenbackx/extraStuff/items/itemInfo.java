package rubenbackx.extraStuff.items;

public class ItemInfo {
	
	public static final String TEXTURE_LOCATION = "extraStuff";
	
	public static int WAND_ID;
	public static final String WAND_KEY = "Wand";
	public static final int WAND_DEFAULT = 24031;
	public static final String WAND_UNAME = "wand";
	public static final String WAND_NAME = "Wand";
	public static final String WAND_ICON = "wand";
	
	public static int URANIUM_ID;
	public static final String URANIUM_KEY = "Uranium";
	public static final int URANIUM_DEFAULT = 24032;
	public static final String URANIUM_UNAME = "uranium";
	public static final String URANIUM_NAME = "Uranium";
	public static final String URANIUM_ICON = "uranium";
	
	public static int PARTS_ID;
	public static final String PARTS_KEY = "Nuclear bomb parts";
	public static final int PARTS_DEFAULT = 24033;
	public static final String PARTS_UNAME = "nukeparts";
	public static final String PARTS_NAME = "Nuclear Bomb Parts";
	
	public static int LEAD_INGOT_ID;
	public static final String LEAD_KEY = "Lead ingot";
	public static final int LEAD_DEFAULT = 24034;
	public static final String LEAD_UNAME = "leadingot";
	public static final String LEAD_NAME = "Lead Ingot";

	public static int LEAD_PLATE_ID;
	public static final String LEAD_PLATE_KEY = "Lead plate";
	public static final int LEAD_PLATE_DEFAULT = 24035;
	public static final String LEAD_PLATE_UNAME = "leadplate";
	public static final String LEAD_PLATE_NAME = "Lead Plate";
	
}
