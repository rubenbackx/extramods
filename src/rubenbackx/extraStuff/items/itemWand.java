package rubenbackx.extraStuff.items;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import rubenbackx.extraStuff.blocks.Blocks;
import rubenbackx.extraStuff.lib.Misc;
import rubenbackx.extraStuff.lib.Players;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemWand extends ItemTool implements Icon{

	private static Block[] emptyBlockArray = {};
	private static String owner;
	
	public ItemWand(int id, EnumToolMaterial material) {
		super(id, 0, material, emptyBlockArray);
		setUnlocalizedName(ItemInfo.WAND_UNAME);
		setMaxStackSize(1);
		setContainerItem(this);
	}
	
	@Override
	public void onCreated(ItemStack stack, World world, EntityPlayer player){
		
		stack.stackTagCompound = new NBTTagCompound();
		
		stack.stackTagCompound.setString("owner", player.username);
		
		
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean idk){
		
		if(stack.stackTagCompound != null){
			
			owner = stack.stackTagCompound.getString("owner");
			
			list.add("Owner: " + owner);
			
		}else{
			
			stack.stackTagCompound = new NBTTagCompound();
			
			stack.stackTagCompound.setString("owner", player.username);
			
			owner = stack.stackTagCompound.getString("owner");
			
			list.add("Owner: " + owner);
			
		}
		
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		itemIcon = register.registerIcon(ItemInfo.TEXTURE_LOCATION + ":" + ItemInfo.WAND_ICON);
		
	}
	
	@Override
	public boolean itemInteractionForEntity(ItemStack stack, EntityPlayer player, EntityLivingBase target) {
		
		Random rand = new Random();
		int randomNum = rand.nextInt(2) + 1;
		
		if(!target.worldObj.isRemote){ 
			if(stack.stackTagCompound != null){
				if(Minecraft.getMinecraft().thePlayer.username.equals(owner)){
					
					if(player.isSneaking()){
						target.motionY = 3;
						if(randomNum == 1){
							target.motionZ = 3;
						}else{
							target.motionX = 3;
						}
					}else if(player.isBurning()){
						target.isDead = true;
					}else if(player.isSprinting()){
						player.heal(20F);
						target.isDead = true;
					}else if(player.isInWater()){
						target.setHealth(1);
					}else if(target.worldObj.isRaining()){
						target.worldObj.spawnEntityInWorld(new EntityLightningBolt(target.worldObj, target.posX, target.posY, target.posZ));
					}else{
						target.motionY = 3;	
					}
					
					for(int i = 0; i <= Players.diamondPlayers.length - 1; i++){
						
						if(Minecraft.getMinecraft().thePlayer.username.equals(Players.diamondPlayers[i])){
							
							target.worldObj.spawnEntityInWorld(new EntityItem(target.worldObj, target.posX, target.posY, target.posZ, new ItemStack(Item.diamond)));
							
						}
						
					}
					
					stack.damageItem(1, target);
					
				}
			}
		}
		
		return false;
		
	}
	
	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ){

		String owner = stack.stackTagCompound.getString("owner");
		
		if(!world.isRemote && Minecraft.getMinecraft().thePlayer.username.equals(owner)){
			
			if(!player.isSneaking()){
				switch(side){
					case 1:
						this.setWand(world, x, y+1, z, side);
						break;
					case 2:
						this.setWand(world, x, y, z-1, side);
						break;
					case 3:
						this.setWand(world, x, y, z+1, side);
						break;
					case 4:
						this.setWand(world, x-1, y, z, side);
						break;
					case 5:
						this.setWand(world, x+1, y, z, side);
						break;
					default:
						this.setWand(world, x, y+1, z, side);
						break;
				}
				
				stack.stackSize--;
				
				//System.out.println(side);
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	@Override
	public EnumRarity getRarity(ItemStack itemStack){
		
		return EnumRarity.epic;
		
	}
	
	private void setWand(World world, int x, int y, int z, int side){
		
		boolean isAir = false;
		
		if(world.isAirBlock(x, y, z)){
			isAir = true;
		}
		
		//System.out.println("there's a air block at " + x + " " + y + " " + z + " = " + isAir);
		
		if(isAir){
			world.setBlock(x, y, z, Blocks.wand.blockID);
		}
		
		
	}

	@Override
	public int getIconWidth() {
		return 64;
	}

	@Override
	public int getIconHeight() {
		return 64;
	}

	@Override
	public float getMinU() {
		return 0;
	}

	@Override
	public float getMaxU() {
		return 0;
	}

	@Override
	public float getInterpolatedU(double d0) {
		return 0;
	}

	@Override
	public float getMinV() {
		return 0;
	}

	@Override
	public float getMaxV() {
		return 0;
	}

	@Override
	public float getInterpolatedV(double d0) {
		return 0;
	}

	@Override
	public String getIconName() {
		return null;
	}
	
}	

