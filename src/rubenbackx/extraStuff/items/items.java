package rubenbackx.extraStuff.items;

import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class Items {
	
	public static Item wand;
	public static Item uranium;
//	public static Item nukeParts;
	public static Item leadIngot;
	public static Item leadPlate;
	
	public static EnumToolMaterial WANDMATERIAL;
	
	public static void init(){
		
		WANDMATERIAL = EnumHelper.addToolMaterial("WANDMATERIAL", 0, 10000, 1, 0, 0);
		
		wand = new ItemWand(ItemInfo.WAND_ID, WANDMATERIAL);
		uranium = new ItemUranium(ItemInfo.URANIUM_ID);
//		nukeParts = new StandardItem(ItemInfo.PARTS_ID).setUnlocalizedName(ItemInfo.PARTS_UNAME);
		leadIngot = new StandardItem(ItemInfo.LEAD_INGOT_ID).setUnlocalizedName(ItemInfo.LEAD_UNAME);
		leadPlate = new StandardItem(ItemInfo.LEAD_PLATE_ID).setUnlocalizedName(ItemInfo.LEAD_PLATE_UNAME);
		
		LanguageRegistry.addName(wand, ItemInfo.WAND_NAME);
		GameRegistry.registerItem(wand, ItemInfo.WAND_UNAME);
		LanguageRegistry.addName(uranium, ItemInfo.URANIUM_NAME);
		GameRegistry.registerItem(uranium, ItemInfo.URANIUM_UNAME);
//		LanguageRegistry.addName(nukeParts, ItemInfo.PARTS_NAME);
//		GameRegistry.registerItem(nukeParts, ItemInfo.PARTS_UNAME);
		LanguageRegistry.addName(leadIngot, ItemInfo.LEAD_NAME);
		GameRegistry.registerItem(leadIngot, ItemInfo.LEAD_UNAME);
		LanguageRegistry.addName(leadPlate, ItemInfo.LEAD_PLATE_NAME);
		GameRegistry.registerItem(leadPlate, ItemInfo.LEAD_PLATE_UNAME);
		
		OreDictionary.registerOre("gemUranium", new ItemStack(uranium));
		OreDictionary.registerOre("ingotLead", new ItemStack(leadIngot));
		OreDictionary.registerOre("plateLead", new ItemStack(leadPlate));
		
	}
	

}
