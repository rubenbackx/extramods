package rubenbackx.extraStuff.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemUranium extends Item implements Icon {

	public ItemUranium(int id) {
		super(id);
		setUnlocalizedName(ItemInfo.URANIUM_UNAME);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		itemIcon = register.registerIcon(ItemInfo.TEXTURE_LOCATION + ":" + ItemInfo.URANIUM_ICON);
		
	}
	
	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int slot, boolean idk) {
		
		EntityPlayer player = (EntityPlayer) entity;
		
		if(!entity.isInWater()){
			player.addPotionEffect(new PotionEffect(Potion.poison.id, 50, 5));
		}
		
	}

	@Override
	public int getIconWidth() {
		return 32;
	}

	@Override
	public int getIconHeight() {
		return 32;
	}

	@Override
	public float getMinU() {
		return 0;
	}

	@Override
	public float getMaxU() {
		return 0;
	}

	@Override
	public float getInterpolatedU(double d0) {
		return 0;
	}

	@Override
	public float getMinV() {
		return 0;
	}

	@Override
	public float getMaxV() {
		return 0;
	}

	@Override
	public float getInterpolatedV(double d0) {
		return 0;
	}

	@Override
	public String getIconName() {
		return null;
	}
	
}
