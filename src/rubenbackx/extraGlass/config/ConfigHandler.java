package rubenbackx.extraGlass.config;

import java.io.File;

import net.minecraftforge.common.Configuration;
import rubenbackx.extraGlass.blocks.BlockInfo;
import rubenbackx.extraGlass.items.ItemInfo;

public class ConfigHandler {
	
	public static void init(File file){
		
		Configuration config = new Configuration(file);
		
		config.load();
		
		BlockInfo.waterCubeId = config.getBlock(BlockInfo.waterCubeKey, BlockInfo.waterCubeDefault).getInt() + 256;
		
		ItemInfo.hammerId = config.getItem(ItemInfo.hammerKey, ItemInfo.hammerDefault).getInt() + 256;
		ItemInfo.glassshardId = config.getItem(ItemInfo.glassshardKey, ItemInfo.glassshardDefault).getInt() + 256;
		
		config.save();
		
	}

}
