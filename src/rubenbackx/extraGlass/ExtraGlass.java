package rubenbackx.extraGlass;

import rubenbackx.extraGlass.blocks.Blocks;
import rubenbackx.extraGlass.config.ConfigHandler;
import rubenbackx.extraGlass.creative.Creativetabs;
import rubenbackx.extraGlass.items.Items;
import rubenbackx.extraGlass.lib.Modstuff;
import rubenbackx.extraGlass.proxy.CommonProxy;
import rubenbackx.extraGlass.recipes.RecipesList;
import rubenbackx.extraGlass.serverSide.PacketHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = Modstuff.MOD_ID, name = Modstuff.MOD_NAME, version = Modstuff.MOD_VERSION)
@NetworkMod(channels = {Modstuff.MOD_CHANNEL}, clientSideRequired = Modstuff.MOD_CLIENTSIDEREQ, 
            serverSideRequired = Modstuff.MOD_SERVERSIDEREQ, packetHandler = PacketHandler.class)

public class ExtraGlass {

	@Instance(Modstuff.MOD_ID)
	public static ExtraGlass instance;
	
	@SidedProxy(clientSide = "rubenbackx.extraGlass.proxy.ClientProxy", serverSide = "rubenbackx.extraGlass.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) { 
		
		ConfigHandler.init(event.getSuggestedConfigurationFile());
		
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
	
		Blocks.init();
		Items.init();
		RecipesList.init();
		Creativetabs.init();
		
		proxy.initRenderers();
		
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
	
}
