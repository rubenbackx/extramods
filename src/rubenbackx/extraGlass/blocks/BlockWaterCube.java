package rubenbackx.extraGlass.blocks;

import rubenbackx.extraGlass.tileEntity.TileEntityWaterCube;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockWaterCube extends BlockContainer{

	protected BlockWaterCube(int id) {
		super(id, Material.glass);
		setUnlocalizedName(BlockInfo.waterCubeUName);
		setCreativeTab(CreativeTabs.tabBlock);
		setHardness(0.5F);
	}
	
    @Override
    public int getRenderType() {
            return -1;
    }
    
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    @Override
    public boolean renderAsNormalBlock() {
            return false;
    }

	@Override
	public void registerIcons(IconRegister register){
		
		blockIcon = register.registerIcon(BlockInfo.TEXTURE_LOCATION + ":" + BlockInfo.waterCubeIcon);
		
	}

    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
    {
    	
    	int emptySlot = 999;
    	
    	for(int i = 0; i <= 35; i++){
    		
    		if(player.inventory.mainInventory[i] == null){
    			
    			emptySlot = i;
    			System.out.println(i);
    			
    		}
    		
    	}
    	
    	System.out.println(emptySlot);
    	
    	if(player.inventory.mainInventory[player.inventory.currentItem] != null){
    		
    		if(player.inventory.mainInventory[player.inventory.currentItem].itemID == Item.bucketWater.itemID && world.getBlockMetadata(x, y, z) == 0){
    		
    			if(!player.capabilities.isCreativeMode){
    			
	    	    	world.setBlockMetadataWithNotify(x, y, z, 1, 3);
	    	    	
	    	    	player.inventory.mainInventory[player.inventory.currentItem].itemID = Item.bucketEmpty.itemID;
	    	    	
	    	        return true;
    	        
    			}else{
    				
    				world.setBlockMetadataWithNotify(x, y, z, 1, 3);
	    	    	
	    	        return true;
    				
    			}
    			
    		}else if(player.inventory.mainInventory[player.inventory.currentItem].itemID == Item.bucketEmpty.itemID && world.getBlockMetadata(x, y, z) == 1){
    			
    			if(!player.capabilities.isCreativeMode){
    			
    				player.inventory.mainInventory[player.inventory.currentItem].stackSize--;
    				
	    			if(emptySlot == 999){
	    				
	    				player.dropPlayerItem(new ItemStack(Item.bucketWater));
	    				
	    			}else{
	    				
	    				player.inventory.addItemStackToInventory(new ItemStack(Item.bucketWater));
	    				
	    			}
    			
    			}
    			
    	        return true;
    	    
    		}else{
    			
    			return false;
    			
    		}
    		
    	}else{
    		
    		return false;
    		
    	}
        
    }
    
	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityWaterCube();
	}
	
	

}
