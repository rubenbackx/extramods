package rubenbackx.extraGlass.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockGlass;
import net.minecraft.block.material.Material;

public class Blocks {
	
	public static Block waterCube;
	
	public static void init(){
		
		waterCube = new BlockWaterCube(BlockInfo.waterCubeId);
		
		GameRegistry.registerBlock(waterCube, BlockInfo.waterCubeUName);
		
		LanguageRegistry.addName(waterCube, BlockInfo.waterCubeName);
		
	}
	
}
