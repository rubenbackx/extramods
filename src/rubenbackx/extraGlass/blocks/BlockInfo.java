package rubenbackx.extraGlass.blocks;

public class BlockInfo{

	public static final String TEXTURE_LOCATION = "extraGlass";
	
	public static int waterCubeId;
	public static final String waterCubeKey = "Water cube";
	public static final int waterCubeDefault = 1400;
	public static final String waterCubeUName = "watercube";
	public static final String waterCubeName = "Infinite Water Cube";
	public static final String waterCubeIcon = "waterCube";
	
}
