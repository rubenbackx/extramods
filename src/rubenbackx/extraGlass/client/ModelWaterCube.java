package rubenbackx.extraGlass.client;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelWaterCube extends ModelBase
{
  //fields
    ModelRenderer top1;
    ModelRenderer top2;
    ModelRenderer top3;
    ModelRenderer top4;
    ModelRenderer side1;
    ModelRenderer side2;
    ModelRenderer side3;
    ModelRenderer side4;
    ModelRenderer bot1;
    ModelRenderer bot2;
    ModelRenderer bot3;
    ModelRenderer bot4;
    ModelRenderer sideBig1;
    ModelRenderer sideBig2;
    ModelRenderer sideBig3;
    ModelRenderer sideBig4;
    ModelRenderer topBig;
    ModelRenderer botBig;
    ModelRenderer inside;
  
  public ModelWaterCube()
  {
    textureWidth = 64;
    textureHeight = 64;
    
      top1 = new ModelRenderer(this, 0, 0);
      top1.addBox(0F, 0F, 0F, 16, 3, 3);
      top1.setRotationPoint(-8F, 8F, 5F);
      top1.setTextureSize(64, 64);
      top1.mirror = true;
      setRotation(top1, 0F, 0F, 0F);
      top2 = new ModelRenderer(this, 0, 0);
      top2.addBox(0F, 0F, 0F, 16, 3, 3);
      top2.setRotationPoint(-8F, 8F, -8F);
      top2.setTextureSize(64, 64);
      top2.mirror = true;
      setRotation(top2, 0F, 0F, 0F);
      top3 = new ModelRenderer(this, 0, 0);
      top3.addBox(0F, 0F, 0F, 3, 3, 10);
      top3.setRotationPoint(5F, 8F, -5F);
      top3.setTextureSize(64, 64);
      top3.mirror = true;
      setRotation(top3, 0F, 0F, 0F);
      top4 = new ModelRenderer(this, 0, 0);
      top4.addBox(0F, 0F, 0F, 3, 3, 10);
      top4.setRotationPoint(-8F, 8F, -5F);
      top4.setTextureSize(64, 64);
      top4.mirror = true;
      setRotation(top4, 0F, 0F, 0F);
      side1 = new ModelRenderer(this, 0, 0);
      side1.addBox(0F, 0F, 0F, 3, 10, 3);
      side1.setRotationPoint(5F, 11F, 5F);
      side1.setTextureSize(64, 64);
      side1.mirror = true;
      setRotation(side1, 0F, 0F, 0F);
      side2 = new ModelRenderer(this, 0, 0);
      side2.addBox(0F, 0F, 0F, 3, 10, 3);
      side2.setRotationPoint(5F, 11F, -8F);
      side2.setTextureSize(64, 64);
      side2.mirror = true;
      setRotation(side2, 0F, 0F, 0F);
      side3 = new ModelRenderer(this, 0, 0);
      side3.addBox(0F, 0F, 0F, 3, 10, 3);
      side3.setRotationPoint(-8F, 11F, 5F);
      side3.setTextureSize(64, 64);
      side3.mirror = true;
      setRotation(side3, 0F, 0F, 0F);
      side4 = new ModelRenderer(this, 0, 0);
      side4.addBox(0F, 0F, 0F, 3, 10, 3);
      side4.setRotationPoint(-8F, 11F, -8F);
      side4.setTextureSize(64, 64);
      side4.mirror = true;
      setRotation(side4, 0F, 0F, 0F);
      bot1 = new ModelRenderer(this, 0, 0);
      bot1.addBox(0F, 0F, 0F, 16, 3, 3);
      bot1.setRotationPoint(-8F, 21F, -8F);
      bot1.setTextureSize(64, 64);
      bot1.mirror = true;
      setRotation(bot1, 0F, 0F, 0F);
      bot2 = new ModelRenderer(this, 0, 0);
      bot2.addBox(0F, 0F, 0F, 16, 3, 3);
      bot2.setRotationPoint(-8F, 21F, 5F);
      bot2.setTextureSize(64, 64);
      bot2.mirror = true;
      setRotation(bot2, 0F, 0F, 0F);
      bot3 = new ModelRenderer(this, 0, 0);
      bot3.addBox(0F, 0F, 0F, 3, 3, 10);
      bot3.setRotationPoint(-8F, 21F, -5F);
      bot3.setTextureSize(64, 64);
      bot3.mirror = true;
      setRotation(bot3, 0F, 0F, 0F);
      bot4 = new ModelRenderer(this, 0, 0);
      bot4.addBox(0F, 0F, 0F, 3, 3, 10);
      bot4.setRotationPoint(5F, 21F, -5F);
      bot4.setTextureSize(64, 64);
      bot4.mirror = true;
      setRotation(bot4, 0F, 0F, 0F);
      sideBig1 = new ModelRenderer(this, 42, 0);
      sideBig1.addBox(0F, 0F, 0F, 10, 10, 1);
      sideBig1.setRotationPoint(-5F, 11F, 5F);
      sideBig1.setTextureSize(64, 64);
      sideBig1.mirror = true;
      setRotation(sideBig1, 0F, 0F, 0F);
      sideBig2 = new ModelRenderer(this, 42, 0);
      sideBig2.addBox(0F, 0F, 0F, 10, 10, 1);
      sideBig2.setRotationPoint(-5F, 11F, -6F);
      sideBig2.setTextureSize(64, 64);
      sideBig2.mirror = true;
      setRotation(sideBig2, 0F, 0F, 0F);
      sideBig3 = new ModelRenderer(this, 42, 0);
      sideBig3.addBox(0F, 0F, 0F, 1, 10, 10);
      sideBig3.setRotationPoint(-6F, 11F, -5F);
      sideBig3.setTextureSize(64, 64);
      sideBig3.mirror = true;
      setRotation(sideBig3, 0F, 0F, 0F);
      sideBig4 = new ModelRenderer(this, 42, 0);
      sideBig4.addBox(0F, 0F, 0F, 1, 10, 10);
      sideBig4.setRotationPoint(5F, 11F, -5F);
      sideBig4.setTextureSize(64, 64);
      sideBig4.mirror = true;
      setRotation(sideBig4, 0F, 0F, 0F);
      topBig = new ModelRenderer(this, 24, 20);
      topBig.addBox(0F, 0F, 0F, 10, 1, 10);
      topBig.setRotationPoint(-5F, 10F, -5F);
      topBig.setTextureSize(64, 64);
      topBig.mirror = true;
      setRotation(topBig, 0F, 0F, 0F);
      botBig = new ModelRenderer(this, 24, 20);
      botBig.addBox(0F, 0F, 0F, 10, 1, 10);
      botBig.setRotationPoint(-5F, 21F, -5F);
      botBig.setTextureSize(64, 64);
      botBig.mirror = true;
      setRotation(botBig, 0F, 0F, 0F);
      inside = new ModelRenderer(this, 0, 32);
      inside.addBox(0F, 0F, 0F, 10, 10, 10);
      inside.setRotationPoint(-5F, 11F, -5F);
      inside.setTextureSize(64, 64);
      inside.mirror = true;
      setRotation(inside, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    top1.render(f5);
    top2.render(f5);
    top3.render(f5);
    top4.render(f5);
    side1.render(f5);
    side2.render(f5);
    side3.render(f5);
    side4.render(f5);
    bot1.render(f5);
    bot2.render(f5);
    bot3.render(f5);
    bot4.render(f5);
    sideBig1.render(f5);
    sideBig2.render(f5);
    sideBig3.render(f5);
    sideBig4.render(f5);
    topBig.render(f5);
    botBig.render(f5);
    inside.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
