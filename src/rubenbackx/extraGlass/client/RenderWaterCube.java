package rubenbackx.extraGlass.client;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderWaterCube extends TileEntitySpecialRenderer {
        
        private final ModelWaterCube model;
        public static final ResourceLocation fullLocation = new ResourceLocation("extraglass", "textures/blocks/waterCubeFull.png");
        public static final ResourceLocation emptyLocation = new ResourceLocation("extraglass", "textures/blocks/waterCubeEmpty.png");
        
        public RenderWaterCube() {
                this.model = new ModelWaterCube();
        }
        
        @Override
        public void renderTileEntityAt(TileEntity te, double x, double y, double z, float scale) {
        	GL11.glPushMatrix();
        	
            GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
            
            if(te.getBlockMetadata() == 0){
            	bindTexture(this.emptyLocation); 
            }else{
            	bindTexture(this.fullLocation);
            }
            
            GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
            
            this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
            
            GL11.glPopMatrix();
        }
}