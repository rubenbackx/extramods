package rubenbackx.extraGlass.creative;

import rubenbackx.extraGlass.blocks.Blocks;
import rubenbackx.extraGlass.items.Items;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class Creativetabs {
	
	public static CreativeTabs tabExtraGlass;
	
	public static void init(){
		
        tabExtraGlass = new CreativeTabs("tabExtraGlass") {
            public ItemStack getIconItemStack() {
                    return new ItemStack(Items.glassshard, 1, 0);
            }
        };
		
		LanguageRegistry.instance().addStringLocalization("itemGroup.tabExtraGlass", "en_US", "Extra Glass++");
		
		Blocks.waterCube.setCreativeTab(tabExtraGlass);
		Items.glassshard.setCreativeTab(tabExtraGlass);
		Items.hammer.setCreativeTab(tabExtraGlass);
		
	}

}
