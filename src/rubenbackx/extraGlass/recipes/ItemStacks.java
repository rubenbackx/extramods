package rubenbackx.extraGlass.recipes;

import rubenbackx.extraGlass.items.Items;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemStacks {
	
	public static final ItemStack stickStack = new ItemStack(Item.stick);
	public static final ItemStack ironStack = new ItemStack(Item.ingotIron);
	public static final ItemStack paneStack = new ItemStack(Block.thinGlass);
	public static final ItemStack shardStack = new ItemStack(Items.glassshard);

}
