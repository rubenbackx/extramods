package rubenbackx.extraGlass.recipes;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import rubenbackx.extraGlass.blocks.Blocks;
import rubenbackx.extraGlass.items.Items;
import cpw.mods.fml.common.registry.GameRegistry;

public class RecipesList {

	public static void init(){
		
	    GameRegistry.addRecipe(new ShapedOreRecipe(Items.hammer, true, new Object[]{"xxx",
				                                                                    "x|x", 
				                                                                    " | ", 
		Character.valueOf('x'), "plateLead", Character.valueOf('|'), ItemStacks.stickStack}));
	    
	    GameRegistry.addRecipe(new ShapedOreRecipe(Blocks.waterCube, true, new Object[]{"xox",
	    																			    "o-o", 
	    																			    "xox", 
        Character.valueOf('x'), "plateLead", Character.valueOf('o'), ItemStacks.ironStack, Character.valueOf('-'), ItemStacks.paneStack}));
		
	    GameRegistry.addRecipe(new ItemStack(Block.glass, 1), "xxx",
	    													  "xxx",
	    													  "xxx",
	    'x', ItemStacks.shardStack);
	    
	}

}
