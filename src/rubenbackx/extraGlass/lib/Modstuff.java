package rubenbackx.extraGlass.lib;

public class Modstuff {
	
	//Mod
	public static final String MOD_ID = "ExtraGlass";
	public static final String MOD_NAME = "Extra Glass++";
	public static final String MOD_VERSION = "Beta";
	
	//Networkmod
	public static final String MOD_CHANNEL = "ExtraGlass";
	public static final boolean MOD_CLIENTSIDEREQ = true;
	public static final boolean MOD_SERVERSIDEREQ = false;

}
