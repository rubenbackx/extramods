package rubenbackx.extraGlass.proxy;

import rubenbackx.extraGlass.blocks.BlockInfo;
import rubenbackx.extraGlass.client.RenderWaterCube;
import rubenbackx.extraGlass.client.RenderWaterCubeItem;
import rubenbackx.extraGlass.tileEntity.TileEntityWaterCube;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.client.registry.ClientRegistry;

public class ClientProxy extends CommonProxy{

	@Override
	public void initRenderers(){
		
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWaterCube.class, new RenderWaterCube());
		MinecraftForgeClient.registerItemRenderer(BlockInfo.waterCubeId, new RenderWaterCubeItem());
		
	}
	
}
