package rubenbackx.extraGlass.items;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemHammer extends ItemTool{
	
	public static final Block[] blocksEffectiveAgainst = {Block.cobblestone, Block.stoneDoubleSlab, Block.stoneSingleSlab, Block.stone, Block.sandStone, Block.cobblestoneMossy, Block.oreIron, Block.blockIron, Block.oreCoal, Block.blockGold, Block.oreGold, Block.oreDiamond, Block.blockDiamond, Block.ice, Block.netherrack, Block.oreLapis, Block.blockLapis, Block.oreRedstone, Block.oreRedstoneGlowing, Block.rail, Block.railDetector, Block.railPowered, Block.railActivator};

	protected ItemHammer(int id, EnumToolMaterial material) {
		super(id, 2, material, blocksEffectiveAgainst);
		setUnlocalizedName(ItemInfo.hammerUName);
		setCreativeTab(CreativeTabs.tabTools);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		itemIcon = register.registerIcon(ItemInfo.TEXTURE_LOCATION + ":" + ItemInfo.hammerIcon);
		
	}
	
	@Override
	public boolean onBlockStartBreak(ItemStack stack, int x, int y, int z, EntityPlayer player){
		
		if(!player.worldObj.isRemote){
			if(Block.blocksList[player.worldObj.getBlockId(x, y, z)].blockMaterial == Material.glass){
				if(!player.capabilities.isCreativeMode){
					
					player.worldObj.setBlockToAir(x, y, z);
					
					Random rand = new Random();
					int randomNum = rand.nextInt(9) + 1;
					
					for(int i = 0; i <= randomNum; i++){
						
						player.worldObj.spawnEntityInWorld(new EntityItem(player.worldObj, x, y, z, new ItemStack(Items.glassshard)));
						
					}
					
					return true;
				
				}else{
					
					return false;
					
				}
				
			}else{
				
				return false;
				
			}
		}else{
			
			return false;
			
		}
		
	}

}
