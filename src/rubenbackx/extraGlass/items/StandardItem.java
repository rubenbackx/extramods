package rubenbackx.extraGlass.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class StandardItem extends Item{

	public StandardItem(int id) {
		super(id);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register){
		
		String name = this.getUnlocalizedName();
		String texturename = name.replace("item.", "");
		
		itemIcon = register.registerIcon(ItemInfo.TEXTURE_LOCATION + ":" + texturename);
		
	}
	
}
