package rubenbackx.extraGlass.items;

import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;

public class ItemInfo {
	
	public static final String TEXTURE_LOCATION = "extraGlass";
	
	public static int hammerId;
	public static final int hammerDefault = 12000;
	public static final String hammerName = "Hammer";
	public static final String hammerIcon = "hammer";
	public static final String hammerKey = "Hammer";
	public static final String hammerUName = "hammer";
	
	public static int glassshardId;
	public static final int glassshardDefault = 12001;
	public static final String glassshardName = "Shard of Glass";
	public static final String glassshardKey = "Shard of glass";
	public static final String glassshardUName = "glassshard";
	
}
