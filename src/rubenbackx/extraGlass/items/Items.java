package rubenbackx.extraGlass.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.MinecraftForge;

public class Items {
	
	public static Item hammer;
	public static Item glassshard;
	
	public static EnumToolMaterial HAMMERMATERIAL;
	
	public static void init(){
		
		HAMMERMATERIAL = EnumHelper.addToolMaterial("HAMMERMATERIAL", 2, 900, 2.5F, 4, 0);
		
		hammer = new ItemHammer(ItemInfo.hammerId, HAMMERMATERIAL);
		glassshard = new StandardItem(ItemInfo.glassshardId).setUnlocalizedName(ItemInfo.glassshardUName).setCreativeTab(CreativeTabs.tabMaterials);
		
		LanguageRegistry.addName(hammer, ItemInfo.hammerName);
		LanguageRegistry.addName(glassshard, ItemInfo.glassshardName);
		
		GameRegistry.registerItem(hammer, ItemInfo.hammerUName);
		GameRegistry.registerItem(glassshard, ItemInfo.glassshardUName);
		
		MinecraftForge.setToolClass(hammer, "pickaxe", 2);
		
	}
	
}
