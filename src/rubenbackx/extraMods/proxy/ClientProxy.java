package rubenbackx.extraMods.proxy;

import net.minecraftforge.client.MinecraftForgeClient;
import rubenbackx.extraStuff.blocks.BlockInfo;
import rubenbackx.extraStuff.client.RenderCreeperBlock;
import rubenbackx.extraStuff.client.RenderCreeperBlockItem;
import rubenbackx.extraStuff.client.RenderNuclearBomb;
import rubenbackx.extraStuff.client.RenderNuclearBombItem;
import rubenbackx.extraStuff.client.RenderWand;
import rubenbackx.extraStuff.client.RenderWandItem;
import rubenbackx.extraStuff.tileEntities.TileEntityCreeperBlock;
import rubenbackx.extraStuff.tileEntities.TileEntityNuclearBomb;
import rubenbackx.extraStuff.tileEntities.TileEntityWand;
import cpw.mods.fml.client.registry.ClientRegistry;


public class ClientProxy extends CommonProxy{
	
	@Override
	public void initSounds() {
		
	}
	
	@Override
	public void initRenderers() {

	}

}
