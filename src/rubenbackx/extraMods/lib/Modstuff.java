package rubenbackx.extraMods.lib;

public class Modstuff {
	
	//Mod
	public static final String MOD_ID = "ExtraMods";
	public static final String MOD_NAME = "Extra Mods++";
	public static final String MOD_VERSION = "Beta";
	
	//Networkmod
	public static final String MOD_CHANNEL = "ExtraMods";
	public static final boolean MOD_CLIENTSIDEREQ = true;
	public static final boolean MOD_SERVERSIDEREQ = false;

}
