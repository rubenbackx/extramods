package rubenbackx.extraMods;

import rubenbackx.extraMods.lib.Modstuff;
import rubenbackx.extraMods.proxy.CommonProxy;
import rubenbackx.extraMods.server.PacketHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = Modstuff.MOD_ID, name = Modstuff.MOD_NAME, version = Modstuff.MOD_NAME)
@NetworkMod(channels = {Modstuff.MOD_CHANNEL}, clientSideRequired = Modstuff.MOD_CLIENTSIDEREQ, 
            serverSideRequired = Modstuff.MOD_SERVERSIDEREQ, packetHandler = PacketHandler.class)

public class ExtraMods {

	@Instance("ExtraMods")
	public static ExtraMods instance;

	@SidedProxy(clientSide = "rubenbackx.extraMods.proxy.ClientProxy", serverSide = "rubenbackx.extraMods.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) { 
		
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
	
}
